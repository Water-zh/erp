# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.19-MariaDB)
# Database: SIGMA
# Generation Time: 2017-12-13 02:26:47 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table firmscore_details
# ------------------------------------------------------------

DROP TABLE IF EXISTS `firmscore_details`;

CREATE TABLE `firmscore_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firm_id` int(11) NOT NULL COMMENT '[fk]評比序號',
  `pay_back` int(5) NOT NULL COMMENT '退回數',
  `updatedAt` datetime NOT NULL COMMENT '更新日期',
  `createdAt` datetime NOT NULL COMMENT '新增日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table firmscores
# ------------------------------------------------------------

DROP TABLE IF EXISTS `firmscores`;

CREATE TABLE `firmscores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL COMMENT '[fk]廠商代號',
  `po_number` varchar(20) NOT NULL DEFAULT '' COMMENT 'PONumber',
  `item_id` varchar(20) NOT NULL DEFAULT '' COMMENT '品號',
  `returns` int(10) NOT NULL COMMENT '退回數',
  `note` varchar(200) DEFAULT '' COMMENT '退貨原因',
  `action` varchar(200) DEFAULT '' COMMENT '處置結果',
  `updatedAt` datetime NOT NULL COMMENT '更新時間',
  `createdAt` datetime NOT NULL COMMENT '建立時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
