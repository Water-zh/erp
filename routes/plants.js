var appRouter = express.Router();
var plantClass = require("../controller/plant");
var purchaseClass = require("../controller/purchase");
var plant = new plantClass();
var purchase = new purchaseClass();

//給web版抓工單用
//工單列表
appRouter.post("/get-list", csrfProtection, async (req, res)=>{
    let result = await plant.getList(req);

    result.purchase =await purchase.getListByPO(req);
    res.json(result);
});
appRouter.get("/detail/:orderNo", csrfProtection, async(req, res)=>{
    let result = await plant.getPlantDetail(req.params);
    let backLink = '';
    let count = 0;
    req.session.nowPlant = result;

    for( let k in req.session.selectParams ){
        if( count === 0 ){
            backLink +=`?${k}=${req.session.selectParams[k]}`;
        }else{
            backLink +=`&${k}=${req.session.selectParams[k]}`;
        }
        count++;
    }
    result.backLink = backLink;
    res.render("plant/detail", result);
});
appRouter.get("/part-detail/:orderNo/:partNo", csrfProtection, async(req, res)=>{
    let result = await plant.getPlantPartDetail(req.params);
    result.plant = req.session.nowPlant.result;//工單內容
    result.ortherPlant = await plant.getPartOtherOrder(req.params);//組建其他工單
    result.purchaseList = await purchase.getPartPurchaseList(req.params);//採購紀錄

    for( let k in req.session.nowPlant.detail ){
        let thisDetail = req.session.nowPlant.detail[k];
        if( thisDetail.TB002 == req.params.orderNo && thisDetail.TB003 == req.params.partNo ){
            result.plantDetail = thisDetail;//工單組件內容
        }
    }
    result.params = req.params;
    result._csrf = req.csrfToken();
    result.canEdit = ( req.session.userInfo.department === "PMC" )? true: false;

    res.render("plant/part_detail", result);
});
appRouter.post("/set-purchase-date/", csrfProtection, async (req, res)=>{
    let result = {};
    if( req.body.incomeDate && req.session.userInfo.department === "PMC" ){
        result = await purchase.setStockIncomeDate(req.body);
    }else{
        result.message = "沒有回填交貨日期的權限或交貨日期為空白";
    }

    res.json(result);
});
appRouter.post("/set-params", csrfProtection, (req, res)=>{
    let params = {};
    //page=1&poNo=&start_date=2017-02-06&end_date=2017-02-09&order_status=1
    let keyArray = ["","page","poNo","start_date","end_date","order_status"];
    for( let k in req.body ){
        let thisParam = req.body[k];
        if( keyArray.indexOf( k )>0 ){
            params[k] = thisParam;
        }
    }
    req.session.selectParams = params;
    res.json({ answer : true });
});
// appRouter.post("/upload-files", (req, res)=>{
//     console.log(req.file);
//     console.log(req.body);
// });
module.exports = appRouter;
