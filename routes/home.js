const svgCaptcha = require("svg-captcha");
var appRouter = express.Router();
var type = fileUpload.array("addfile[]", 4);

const baseClass = require("../controller/base");
const userClass = require("../controller/user");
let user = new userClass();
let base = new baseClass();
let filesPath = `./dataFile/`;

appRouter.get("/", csrfProtection, (req, res)=>{
	var captcha = svgCaptcha.create();
    let message = req.session.errMessage || '';
	req.session.errMessage = '';
	req.session.captcha = captcha.text;

  	res.render('login', {
    	_csrf : req.csrfToken(),
		captcha : captcha.data,
		errMessage : message
  	});
});
appRouter.get("/edit",(req, res)=>{
    res.render("editor");
});

appRouter.post("/editPost", type, (req, res)=>{
    console.log(req.body);
    console.log(req.file);
    res.json(req.body);
});
appRouter.get("/logout",csrfProtection ,(req, res)=>{
    let userLog = user.thisUserLogModel();
    //登出紀錄
    userLog.create({
        user_id : req.session.userInfo.id,
        action : "login"
    }).catch(err=>console.log(err) );

    req.session.userInfo = '';
    res.redirect("/");
});

appRouter.post("/login", csrfProtection ,async (req, res)=>{
    if( req.body.captcha !== req.session.captcha || req.body.captcha == '' ){
        req.session.errMessage =  `驗證碼輸入錯誤或空白.`;
        res.redirect('/');
    }else{
        let result = await user.authUser({ account : req.body.username, _password : req.body.Password });
        if( result.answer ){
            req.session.userInfo = result.userInfo;

            res.redirect("/system");
        }else{
            req.session.errMessage = result.message;
            res.redirect("/");
        }
    }
});
appRouter.post("/send-verify", csrfProtection, async (req, res)=>{
    let result = await user.forgetPassword(req.body);
    res.send(result);
});
appRouter.post('/auth-verify',
    csrfProtection,
    async (req, res )=>{
        let checkVerify = await  user.authVerify(req.body);
        res.send( checkVerify );
    }
);
appRouter.post('/reset-password',
    csrfProtection,
    async (req, res )=>{
        let checkVerify = await  user.resetPassword(req.body);
        res.send( checkVerify );
    }
);
module.exports = appRouter;
