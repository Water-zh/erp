var Router = express.Router();

Router.get("/:view", (req, res)=>{
    let { view } = req.params;
    res.render(`manual/${view}`);
});

module.exports = Router;