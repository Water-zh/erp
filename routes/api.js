var bcrypt = require('bcrypt-nodejs');

const userClass = require('../controller/user');
const plantClass = require('../controller/plant');
const purchaseClass = require('../controller/purchase');
const baseClass = require('../controller/base');
const appRouter = express.Router();



let user = new userClass();
let plant = new plantClass();
let base = new baseClass();
let purchase = new purchaseClass();

//---------------帳號相關 api----------------------//

appRouter.use('/*', (req, res, next)=>{
    
    //middleware 處理req中的加密資料
    if( req.body.send_req ){
        req.body = base.unHashPostData( req.body.send_req );
    }
    next();
});

appRouter.post('/user/check-account',
    async (req, res)=>{
        let checkUser = await user.checkUserExist(req.body.account);
        res.send( base.hashResData( checkUser ) );
    }
);
appRouter.post('/user/reset-password',
    async (req, res )=>{
        let resetPassword = await user.resetPassword(req.body);
        res.send( base.hashResData(resetPassword) );
    }
);
appRouter.post('/user/auth-password',
    async ( req, res  )=>{
        let authPassword = await user.authUser( req.body );
        res.send( base.hashResData(authPassword) );
    }
);
appRouter.post('/user/forget-password',
    async (req, res )=>{
        let result = await user.forgetPassword(req.body);
        res.send( base.hashResData(result) );
    }
);
appRouter.post('/user/auth-verify',
    async (req, res )=>{
        let checkVerify = await  user.authVerify(req.body);
        res.send( base.hashResData( checkVerify ) );
    }
);
//---------------工單相關 api----------------------//

appRouter.post('/plant/list',
    async(req, res )=>{
        let plantList = await plant.getPlantListByDate(req.body);
        res.send( base.hashResData( plantList ) );
    }
);
appRouter.post('/plant/list-by-po',
    async(req, res )=>{
        let plantList = await plant.getPlantListByPO(req.body);
        res.send( base.hashResData( plantList ) );
    }
);

appRouter.post('/plant/detail',
    async(req, res )=>{
        let plantDetail = await plant.getPlantDetail(req.body);
        res.send( base.hashResData( plantDetail ) );
    }
);

appRouter.post('/plant/part/detail',
    async(req, res)=>{
        let result = await plant.getPlantPartDetail(req.body);
        res.send( base.hashResData( result ) );
    }
);
appRouter.post('/plant/part/orther-order',
    async(req, res)=>{
        let result = await plant.getPartOtherOrder(req.body);
        res.send( base.hashResData( result ) );
    }
);

appRouter.post('/purchase/list', 
    async (req, res)=>{
        let result = await purchase.getPartPurchaseList(req.body);
        res.send( base.hashResData(result) );
    }
);
appRouter.post('/purchase/set-income-date',
    async (req, res)=>{
        let result = await purchase.setStockIncomeDate(req.body);
        res.send( base.hashResData(result) );
    }
);

module.exports = appRouter;
