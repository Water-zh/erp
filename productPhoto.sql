# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 10.1.13-MariaDB)
# Database: SIGMA
# Generation Time: 2018-01-19 05:44:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table product_design_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_design_categories`;

CREATE TABLE `product_design_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '標題',
  `updatedAt` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_design_categories` WRITE;
/*!40000 ALTER TABLE `product_design_categories` DISABLE KEYS */;

INSERT INTO `product_design_categories` (`id`, `title`, `updatedAt`, `createdAt`)
VALUES
	(1,'簡約','2018-01-10 00:00:00','2018-01-10 00:00:00'),
	(2,'工業','2018-01-10 00:00:00','2018-01-10 00:00:00');

/*!40000 ALTER TABLE `product_design_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_design_photos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_design_photos`;

CREATE TABLE `product_design_photos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `proto_id` int(11) NOT NULL DEFAULT '0' COMMENT '[fk]所屬型號',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '圖片',
  `updatedAt` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_design_photos` WRITE;
/*!40000 ALTER TABLE `product_design_photos` DISABLE KEYS */;

INSERT INTO `product_design_photos` (`id`, `proto_id`, `image`, `updatedAt`, `createdAt`)
VALUES
	(1,1,'6SKIDPw.jpg','2018-01-10 00:00:00','2018-01-10 00:00:00'),
	(2,1,'1-1s.jpg','2018-01-10 00:00:00','2018-01-10 00:00:00'),
	(3,1,'18033244_756924834467171_2744079113770509264_n.jpg','2018-01-10 00:00:00','2018-01-10 00:00:00'),
	(4,2,'6SKIDPw.jpg','2018-01-16 03:04:28','2018-01-16 03:04:28'),
	(5,2,'QnOEYU4.jpg','2018-01-16 03:04:28','2018-01-16 03:04:28'),
	(6,3,'2088070018901b.jpg','2018-01-16 03:34:14','2018-01-16 03:34:14'),
	(7,3,'medish.jpg','2018-01-16 03:34:14','2018-01-16 03:34:14'),
	(8,4,'6SKIDPw.jpg','2018-01-16 06:52:52','2018-01-16 06:52:52');

/*!40000 ALTER TABLE `product_design_photos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product_designs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_designs`;

CREATE TABLE `product_designs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) NOT NULL DEFAULT '0' COMMENT '[fk]所屬類別',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '狀態',
  `title` varchar(30) NOT NULL DEFAULT '' COMMENT '標題',
  `slogan` varchar(50) NOT NULL DEFAULT '' COMMENT '簡述',
  `content` text NOT NULL COMMENT '討論內容',
  `keyword` varchar(50) NOT NULL DEFAULT '' COMMENT '類別關鍵字',
  `updatedAt` datetime NOT NULL COMMENT '更新時間',
  `createdAt` datetime NOT NULL COMMENT '新增時間',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_designs` WRITE;
/*!40000 ALTER TABLE `product_designs` DISABLE KEYS */;

INSERT INTO `product_designs` (`id`, `cate_id`, `status`, `title`, `slogan`, `content`, `keyword`, `updatedAt`, `createdAt`)
VALUES
	(1,1,0,'樣式一','簡約設計','','','2018-01-10 00:00:00','2018-01-10 00:00:00'),
	(2,0,0,'樣式二','帥又簡譜','','簡約','2018-01-16 03:04:28','2018-01-16 03:04:28'),
	(3,0,0,'樣式三','','','','2018-01-16 03:34:14','2018-01-16 03:34:14'),
	(4,0,0,'樣式四','酷酷酷','','簡約','2018-01-16 06:52:52','2018-01-16 06:52:52');

/*!40000 ALTER TABLE `product_designs` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
