module.exports = {
    accounts : {
        GET : ["","index","create","show"],
        POST : ["","store","update","delete"]
    },
    connection : {
        GET : ["","index","create","store","show","download"],
        POST : ["","store","update","delete","keyword","deleteAll"],
    },
    news : {
        GET : ["","index","create","show","download"],
        POST : ["","store","update","delete","register","deleteAll"]
    },
    plant : {
        GET : ["","index","create","show"],
        POST : ["","store","update","delete"]
    },
    firmscore : {
        GET : ["","index","show"],
        POST : ["","searchPurchase","update","score"]
    },
    prototype : {
        GET : ["","index","create","show"],
        POST : ["", "store", "update","search","showProductImages","sendStatus","getMessages","setMessages"]
    }
};