const sequelize = require('sequelize');

module.exports = {
    //工單
    MOCTA :{
        primary : ['TA001','TA002'],
        column : {
            TA001 : { type : sequelize.STRING },//工單別
            TA002 : { type : sequelize.STRING, primaryKey: true },//工單號
            TA003 : { type : sequelize.STRING },//開單日期
            TA004 : { type : sequelize.STRING },//ＢＯＭ日期
            TA005 : { type : sequelize.STRING },//ＢＯＭ板本
            TA006 : { type : sequelize.STRING },//產品品號
            TA007 : { type : sequelize.STRING },//單位
            TA009 : { type : sequelize.STRING },//預計開始
            TA010 : { type : sequelize.STRING },//預計完工
            TA011 : { type : sequelize.STRING },//工單狀態碼
            TA012 : { type : sequelize.STRING },//實際開工
            TA014 : { type : sequelize.STRING },//實際完工
            TA015 : { type : sequelize.STRING },//預計產量
            TA016 : { type : sequelize.STRING },//已領套數
            TA017 : { type : sequelize.STRING },//已生產量
            TA018 : { type : sequelize.STRING },//報廢數量
            TA019 : { type : sequelize.STRING },//工廠編號
            TA033 : { type : sequelize.STRING },//訂單編號
            TA034 : { type : sequelize.STRING },//產品品名
            TA035 : { type : sequelize.STRING },//產品規格
        }
    },
    //工單內容
    MOCTB :{
        primary : ['TB001','TB002'],
        column : {
            TB001 : { type : sequelize.STRING },//工單別
            TB002 : { type : sequelize.STRING },//工單號
            TB003 : { type : sequelize.STRING },//材料品號
            TB004 : { type : sequelize.STRING },//需領用量
            TB005 : { type : sequelize.STRING },//已領用量
            TB007 : { type : sequelize.STRING },//單位
            TB010 : { type : sequelize.STRING },//取代料號
            TB011 : { type : sequelize.STRING },//材料類型
            TB012 : { type : sequelize.STRING },//材料品名
            TB013 : { type : sequelize.STRING },//材料規格
            TB014 : { type : sequelize.STRING },//上階主件品號
            TB015 : { type : sequelize.STRING },//預計領料時間
            TB016 : { type : sequelize.STRING },//實際領料時間
        },
        belong : {
            MOCTA : {
                TA001 : 'TB001',
                TA002 : 'TB002',
            }
        }
    },
    //採購單
    PURTC :{
        primary : ['TC001','TC002'],
        column : {
            TC002 : { type : sequelize.STRING, primaryKey: true },//採購單號
            TC003 : { type : sequelize.DATE },//採購日期
            TC004 : { type : sequelize.STRING },//供應商
        }
    },
    //採購單內容
    PURTD :{
        primary : ['TD001','TD002','TD003'],
        belong : {
            PURTC : {
                TC001 : 'TD001',
                TC002 : 'TD002',
            }
        },
        column : {
            TD001 : { type : sequelize.STRING },//採購單別
            TD002 : { type : sequelize.STRING, primaryKey: true },//採購單號
            TD004 : { type : sequelize.STRING },//品號
            TD005 : { type : sequelize.STRING },//品名
            TD006 : { type : sequelize.STRING },//規格
            TD008 : { type : sequelize.STRING },//採購數量
            TD009 : { type : sequelize.STRING },//單位
            TD012 : { type : sequelize.STRING },//預計交貨日
            TD015 : { type : sequelize.STRING },//已交數量
            TD016 : { type : sequelize.STRING },//結束
            TD024 : { type : sequelize.STRING },//訂單編號
            TD025 : { type : sequelize.STRING },//急料
        }
    },
    //品號
    INVMB :{
        primary : ['MB001'] ,
        column : {
            MB001 : { type : sequelize.STRING },//品號
            MB002 : { type : sequelize.STRING },//品名
            MB003 : { type : sequelize.STRING },//規格
            MB004 : { type : sequelize.STRING },//庫存單位
            MB005 : { type : sequelize.STRING },//品好分類一
            MB006 : { type : sequelize.STRING },//品好分類二
            MB007 : { type : sequelize.STRING },//品好分類三
            MB008 : { type : sequelize.STRING },//品好分類四
            MB064 : { type : sequelize.STRING },//庫存數量
        },
    },
    //品號內容
    INVMC :{
        primary : ['MC001','MC002'],
        column : {
            MC001 : { type : sequelize.STRING },//品號
            MC004 : { type : sequelize.STRING },//安全庫存
            MC005 : { type : sequelize.STRING },//補貨點
            MC007 : { type : sequelize.STRING },//庫存
            MC009 : { type : sequelize.STRING },//標準庫存量
            MC011 : { type : sequelize.STRING },//上次盤點日
        },
        belong : {
            INVMB : {
                MB001 : 'MC001',
            }
        }
    },
    //帳號
    USER : {
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            is_active : { type : sequelize.INTEGER },//帳號是否啟用
            super_user : { type : sequelize.INTEGER },//是否為超級帳號
            is_can_edit : { type : sequelize.INTEGER },//是否有可編輯權限
            ip : { type : sequelize.STRING },//用戶ip
            department : { type : sequelize.STRING },//部門
            account : { type : sequelize.STRING },//帳號
            user_name : { type : sequelize.TEXT },//使用者姓名
            email : { type : sequelize.STRING }, //信箱
            _password : { type : sequelize.STRING }, //密碼
            check_code : { type : sequelize.STRING }, //檢查碼
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    UserLog : {
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            user_id : { type : sequelize.INTEGER },//帳號是否啟用
            action : { type : sequelize.STRING },//是否為超級帳號
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    // ADMMF :{
    //     primary : ['MF001','MF002'],
    //     column : {
    //         MF001 : { type : sequelize.STRING },//帳號
    //         MF002 : { type : sequelize.STRING },//用戶姓名
    //         MF004 : { type : sequelize.STRING },//組編號
    //         MF005 : { type : sequelize.STRING },//超級用戶
    //         MF007 : { type : sequelize.STRING },//部門
    //         MF008 : { type : sequelize.STRING },//類別
    //         UDF01 : { type : sequelize.STRING },//password
    //         UDF02 : { type : sequelize.STRING },//email
    //         UDF03 : { type : sequelize.STRING },//repassword code
    //     }
    // },
    //帳號內容
    ADMMG :{
        primary : ['MG001','MG002','MG009'],
        belong : {
            ADMMF : {
                MF001 : 'MG001',
            }
        }
    },
    DCSMA : {
       column :{
           MA001 : { type : sequelize.STRING },//user code
           MA002 : { type : sequelize.STRING },//user name
           MA006 : { type : sequelize.STRING },//email
           UDF07 : { type : sequelize.STRING },//app password
       }
    },
    //部門資訊 帳號角色
    CMSME :{
        primary : ["ME001","ME002"],
        column : {
            ME001 : { type : sequelize.STRING },//部門編號
            ME002 : { type : sequelize.STRING },//部門名稱
            // ME005 : { type : sequelize.STRING },//超級用戶
        }
    },
    //供應商資訊
    PURMA : {
        column : {
            MA001 : { type : sequelize.STRING, primaryKey: true },//供應商編號
            MA002 : { type : sequelize.STRING },//供應商簡稱
            // MA003 : { type : sequelize.STRING },//供應商公司名稱
        }
    },
    //客戶訂單單頭資訊檔
    COPTC : {
        primary : ['TC001', 'TC002']
    },
    //客戶訂單子內容
    COPTD : {
        primary : ['TD001', 'TD002','TD003'],//TC001+TC002+TC003+TC004
        belong : {
            COPTC : {
                TC001 : 'TD001',
                TC002 : 'TD002',
            }
        }
    },


    //MariaDB 資料庫內容
    //聯絡單
    Conntion : {
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            is_delete :{ type : sequelize.INTEGER },//是否已刪除
            owner : { type : sequelize.INTEGER },//聯絡單發起者
            owner_department : { type : sequelize.STRING },//發起者部門
            title : { type : sequelize.STRING },//聯絡單標題
            content : { type : sequelize.TEXT },//聯絡單內容
            file : { type : sequelize.STRING }, //附加檔案
            keyword : { type : sequelize.STRING }, //關鍵字
            remind_date : { type : sequelize.STRING },//提醒日期
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //聯絡單對象部門
    ConntionDepartment:{
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            connection_id : { type : sequelize.INTEGER },//序號
            department : { type : sequelize.STRING },//對象部門
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //聯絡單觀看紀錄
    ConnectRecord:{
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            connection_id : { type : sequelize.INTEGER },//序號
            user : { type : sequelize.INTEGER },//觀看者帳號
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //聯絡單修改記錄
    ConnectChange:{
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            connection_id : { type : sequelize.INTEGER },//序號
            c_department : { type : sequelize.STRING },//修改的部門
            changer : { type : sequelize.INTEGER },//修改的帳號
            column : { type : sequelize.STRING },//修改的欄位
            status : { type : sequelize.STRING },//新增資料或刪除資料
            record : { type : sequelize.TEXT },//修改內容
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //聯絡單檔案下載記錄
    ConnectLog:{
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            connection_id : { type : sequelize.INTEGER },//序號
            user_id : { type : sequelize.INTEGER },//修改的部門
            action : { type : sequelize.STRING },//修改的帳號
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //公告訊息
    News : {
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            is_check : { type : sequelize.INTEGER },//已經審核
            is_delete : { type : sequelize.INTEGER },//已經刪除
            // owner : { type : sequelize.STRING },//公告訊息發起者
            // owner_department : { type : sequelize.STRING },//發起者部門
            title : { type : sequelize.STRING },//公告訊息標題
            content : { type : sequelize.TEXT },//公告訊息內容
            file : { type : sequelize.STRING }, //附加檔案
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //公告觀看紀錄
    NewsRecord:{
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            news_id : { type : sequelize.INTEGER },//序號
            user : { type : sequelize.INTEGER },//觀看者帳號
            department : { type : sequelize.STRING },
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //公告修改記錄
    NewsChange:{
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            news_id : { type : sequelize.INTEGER },//序號
            changer : { type : sequelize.INTEGER },//修改的帳號
            column : { type : sequelize.STRING },//修改的欄位
            status : { type : sequelize.STRING },//新增資料或刪除資料
            record : { type : sequelize.TEXT },//修改內容
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //公告檔案下載記錄
    NewsLog:{
        primary : ['id'],
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            news_id : { type : sequelize.INTEGER },//序號
            user_id : { type : sequelize.INTEGER },//修改的部門
            action : { type : sequelize.STRING },//修改的帳號
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //供應商評比
    FirmScore : {
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            store_id : { type : sequelize.INTEGER },//供應商編號
            po_number : { type : sequelize.INTEGER },//訂單編號
            item_id : { type : sequelize.STRING },//品號
            returns : { type : sequelize.INTEGER },//退回數
            note : { type : sequelize.TEXT },//退回原因
            action : { type : sequelize.TEXT },//處理結果
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    FirmScoreDetail : {
        column : {
            id : { type : sequelize.INTEGER, primaryKey: true },//序號
            firm_id : { type : sequelize.INTEGER },//評比序號
            pay_back : { type : sequelize.INTEGER },//評比序號
            createdAt : { type : sequelize.DATE } //建立日
        }
    },
    //產品設計
    ProductDesignCategory : {
        id : { type : sequelize.INTEGER, primaryKey: true },//序號
        title : { type : sequelize.STRING },//評比序號
        createdAt : { type : sequelize.DATE } //建立日
    },
    ProductDesign : {
        id : { type : sequelize.INTEGER, primaryKey: true },//序號
        status : { type : sequelize.INTEGER },//狀態
        owner : { type : sequelize.INTEGER },//擁有者
        title : { type : sequelize.STRING },//標題
        keyword : { type : sequelize.STRING },//搜尋關鍵字
        file_path : { type : sequelize.STRING },//產品資料夾
        createdAt : { type : sequelize.DATE } //建立日
    },
    ProductDesignPhotos : {
        id : { type : sequelize.INTEGER, primaryKey: true },//序號
        proto_id : { type : sequelize.INTEGER },//[fk]所屬產品
        image : { type : sequelize.STRING },//圖片
        createdAt : { type : sequelize.DATE } //建立日
    },
    ProductDesignStatusChange : {
        id : { type : sequelize.INTEGER, primaryKey: true },//序號
        proto_id :{ type : sequelize.INTEGER},//[fk]所屬產品設計
        changer : { type : sequelize.INTEGER },//[fk]更改狀態者
        status : { type : sequelize.INTEGER },//所更改的狀態
        createdAt : { type : sequelize.DATE } //建立日
    },
    ProductDesignPermission : {
        id : { type : sequelize.INTEGER, primaryKey: true },//序號
        account :{ type : sequelize.INTEGER},//[fk]使用者帳號
        can_see : { type : sequelize.INTEGER },//[fk]更改狀態者
        is_designer : { type : sequelize.INTEGER },//是設計師
        can_answer : { type : sequelize.INTEGER },//可以點選回應狀態
        createdAt : { type : sequelize.DATE } //建立日
    },
    ProductDesignMessage : {
        id : { type : sequelize.INTEGER, primaryKey: true },//序號
        proto_id : {type : sequelize.INTEGER},//
        account :{ type : sequelize.INTEGER},//[fk]使用者帳號
        message : { type : sequelize.STRING },//[fk]更改狀態者
        createdAt : { type : sequelize.DATE } //建立日
    }
}
