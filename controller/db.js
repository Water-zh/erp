const Sequelize = require('sequelize');
const mysql = require('mysql');
const dataTablePrimary = require('../dataTablePrimary');
//server 端
const serverDBConfig = {
    dialect : "mssql",
    hostname : '192.168.1.8',
    //port : 3306,
    dbName : 'DJERP', //production db -> DJERP, 測試環境-> TEST
    account : 'tw',
    password : 'Tx@201706',
};
//開發端
const hostDBConfig = {
    dialect : "mysql",
    hostname : 'localhost',
    port : 3306,
    dbName : 'homestead', //production db -> YFSVs
    account : 'root',
    password : 'PiglovedoG99',
};

/**
*  * @param {TableName}   DataBase  connect db 
*/

class db{
    constructor( table, colType){
        this.tableName = `${table.toLowerCase()}`;
        //this.tableName = table;
        this.filedsType = colType;
        let dbConfig = ( false )? serverDBConfig : hostDBConfig;
        
        this._seq = new Sequelize(dbConfig.dbName, dbConfig.account, dbConfig.password,{
                host : dbConfig.hostname,
                port: dbConfig.port,
                dialect : dbConfig.dialect,
                pool:{
                    max: 5,
                    min: 0,
                    idle: 15000
                }
            });

        this.thisModel = this._seq.define(
            this.tableName, 
            this.filedsType, 
            { 
                freezeTableName : (true)?true : false,
                timestamps : false
            }
         );
    }


    /** 
     *  * @param {create}   model  create data 將要儲存的欄位和資料用json格式傳入  { title : 'test',is_visible : 1}
    */
    create( data ){
        return this.thisModel.build( data ).save();
    };
    /** 
     *  * @param {update}  model 是 Sequelize find 後的物件,data 是 要update 的資料 { title : 'test', is_visible : 1 }
    */
    update( data, selectWhere){
        return this.thisModel.update(data, { where : selectWhere });
    };
    /** 
     *  * @param {findAllData} 抓取所有資料，如果沒有下篩選就抓全部欄位
    */
    findAllData(fileds){
        let data = this.thisModel.findAll({
            attributes: fileds
        });
        return data;
    };
    /** 
     *  * @param {findOneByWhere} 欄位去找出單筆資料
    */
    findOneByWhere(col , attr){
        return this.thisModel.findAll({ where : col, attributes : attr }).then( data =>{
            return data[0];
        });
    }
    /**
     *  * @param {findOneByWhere} 欄位去找出所有資料
     */
    findAllByWhere(col , attr){
        return this.thisModel.findAll({ where : col, attributes : attr });
    }
    /**
     *  * @param {findOne} 抓單筆資料，用「then」獲取 Promise 資料
    */
    findOne(id){
        return this.thisModel.findById(id);
    }
    /** 
     *  * @param {deleteOne} 刪除單筆資料
    */

    deleteOne (uuid){
        return this.thisModel.destroy({
            where : { id : uuid },
            truncate : true
        });
    }
    /**
     * *@param {checkTableExist} 檢查相同名稱的資料表是否存在
     */
    checkTableExist(dataSheet){
        if( !this.tableName)throw new Error('Not Found TableName, Can not check exist. ');
        
        let account = ( this.tableName === 'admmf' )? ', _password VARCHAR (100) NOT NULL COLLATE utf8_general_ci ' : '';
        let email = ( this.tableName === 'admmf' )? ', email VARCHAR (100) NOT NULL COLLATE utf8_general_ci ' : '';

        let query = `create table if not exists ${this.tableName}(
            id INT auto_increment primary key,
            createdAt DATETIME,
            updatedAt DATETIME ${account} ${email}
        )`;

        this.sheetData = dataSheet.data;
        this.sheetName = dataSheet.name
        this.col_lenght = dataSheet.col_lenght;

        this._seq.query(query).then(()=>{
            this.setTableFileds();
        });

    }
    /**
     * *@param {setTableFileds} 新增目前sheet所需要的資料表  
     */
    setTableFileds(){
        let filedString = '';
        let filedLength = Object.keys( this.filedsType ).length;
        let count = 1;

        //建立欄位資訊，目前先以全字串類型下去處理
        for( let key in this.filedsType ){
            let this_col_len = (this.col_lenght[key] !== 0)?this.col_lenght[key] :3;
            //utf8_general_ci  big5_chinese_ci
            filedString +=` ${ key } VARCHAR (${this_col_len}) NOT NULL COLLATE utf8_general_ci`;
            if( filedLength !== count ) filedString += ',\n';
            count++;
        }
        
        let query = `ALTER TABLE ${this.tableName} ADD COLUMN if not exists ( 
            ${filedString}
        )`;
        this._seq.query(query).then(()=>{
            this.saveDataRow();
        });
    }

    /**
     * *@param {saveSheetData} 儲存文件中的資料
     * 
     */
     saveDataRow(){
        if( this.sheetData.length < 0 ) throw new Error('sheet data row is empty');
        
        for( let c=0; c<this.sheetData.length; c++ ){

            let thisPrimary = dataTablePrimary[ this.sheetName ].primary;
            let selectFiled = {};

            //將要查詢的資料uuid和資料組成物件
            for( let v=0; v<thisPrimary.length; v++ ){
                selectFiled[ thisPrimary[v] ] = this.sheetData[c][ thisPrimary[v] ];
            }
            //先找找看有沒有相同的資料存在
            this.findOneByWhere( selectFiled, thisPrimary ).then(( data )=>{
                
                //不存在就新增，存在就更新
                if( !data ) this.create( this.sheetData[ c ] );
                if( data ) this.thisModel.update(this.sheetData[ c ], { where : selectFiled });
                
            });

        }
     }
};
module.exports = db;
