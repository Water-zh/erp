var userClass = require("./user");
var baseClass = require("./base");
var bcrypt = require('bcrypt-nodejs');

const base = new baseClass();
let user = new userClass();
let userModel = user.thisUserModel();

class account{
    async index(req, res){
        let result = {};
        result.allAccount = await userModel.findAll({ }).then(async data=>{ return data });
        
        res.render("account/index",{result});
    }
    async create(req, res){
        let result = {};
        result.allDepartment = await user.getAllDeparments();
        result._token = req.csrfToken();
        res.render("account/create", {result});            

    }
    store(req, res){
        req.body.super_user = ( req.body.super_user ==="on" )? 1:0;
        req.body.is_active = ( req.body.is_active ==="on" )? 1:0;
        userModel
            .create({
                is_active : req.body.is_active,
                ip : req.body.ip,
                super_user : req.body.super_user,
                department : base.contentReplaceSpace(req.body.department),
                account : req.body.account,
                email : req.body.email,
                user_name : req.body.user_name,
                _password : bcrypt.hashSync( req.body._password ),
            })
            .catch(err=>{ console.error(err) });
        
        res.redirect("/system/accounts");
    }
    async show(req, res){
        let result = {};

        result._token = req.csrfToken();
        result.allDepartment = await user.getAllDeparments();
        result.account = await 
            userModel
            .findOne({
                where : { id : req.params.id }
            })
            .then(data => { return data });

        res.render("account/show", {result});

    }
    update(req, res){
        userModel
        .findOne({
            where : { id : req.body.id }
        })
        .then(data=>{
            let updateRow = {};
            delete req.body._csrf;
            req.body.is_active = ( req.body.is_active === "on" && req.body.is_active )?1 : 0;
            req.body.super_user = ( req.body.super_user === "on" && req.body.super_user)?1 : 0;
            req.body.is_can_edit = ( req.body.is_can_edit === "on" && req.body.is_can_edit)?1 : 0;

            if( req.body._password === "" ){
                delete req.body._password;
            }
            for( let key in req.body ){
                let oldData = data[key];
                let newData = req.body[key];

                if( newData !== oldData && key !== "id" ){
                    if( key === "_password" && newData ){
                        //先檢查是否跟原本密碼依樣，如果不依樣就更新
                        updateRow[key] = bcrypt.hashSync( newData );
                    }else if( key === "deparment" && newdata ){
                        updateRow[key] = newData.trim();
                    }else{
                        updateRow[key] = newData;
                    }
                    
                }
            }
            if( updateRow ){
                userModel.update(updateRow, { where : { id : req.body.id } });
            }
        });
        res.redirect(`/system/accounts/show/${req.body.id}`);
    }
    async delete(req, res){
        let result = {};
        result = await userModel.update({ is_delete : 1 },{ where : { id : req.body.id } })
        .then(async data=> {
            return { answer : true, message : "已刪除帳號" };
        });

        res.json(result);
    }
}

module.exports = account;