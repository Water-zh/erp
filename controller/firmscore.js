const { FirmScore, FirmScoreDetail } = require("../models/firmscore");
const sequelize = require("sequelize");
const base = require("./base");
const db = require("./db")
const dataTablePrimary = require("../dataTablePrimary");
const dateformat = require('dateformat');
const PURTD = app.get("PURTD");
const PURMA = app.get("PURMA");
const PURTC = new db("PURTC", dataTablePrimary.PURTC.column);

var baseClass = new base();
var OP = sequelize.Op;

class firm {
    async index(req, res){
        let result = {};
        result._crsf = req.csrfToken();
		result.data = await this.getList(req);
		result.firms = await this.getFirmName();
		
        res.render("firm/index", result);
    }
    async searchPurchase(req, res){
        let result = await this.getList(req);
        res.json(result);
    }
    async show(req, res){
        let { id } = req.params;
        id = id.split("-");
        let result = await PURTD.thisModel
            .findOne({
                where : {  
                    TD002 : id[0],
                    TD004 : id[1]
                }
            }).then(async list=>{ 
                try {
					//抓取此張採購單的供應商與採購日期
					list.HEAD = await PURTC.thisModel
						.findOne({
							where : { TC002 : id[0] }
						})
						.then( head =>{ return head } );
					//此張採購單的評分記錄
                    list.Score = await FirmScore
                        .findOne({
                            where : {
                                item_id : id[1],
                                po_number : id[0]
                            }
                        }).then(async score=>{
							score.Detail = await score.getDetails();
                            return score;
                        });
                } catch (err) {  }
                return list; 
			});
        res.render("firm/show", { data : result, _crsf : req.csrfToken() });
    }
    async update(req, res){
       let {item_id, po_number } = req.body; 
	   let result = {};
        delete req.body._csrf;//移除token欄位
       if ( item_id && po_number ){
			let where = { item_id ,po_number };

			//抓取該訂單的品號退貨狀況
			result = await FirmScore.findOne({ where })
				.then(async firm =>{
					//存在就只更新「被退貨的補回」
					if( firm ){
						if( req.body.pay_back && !(parseInt(req.body.pay_back) > parseInt(firm.get("returns"))) ){
							//先找有沒有補貨記錄，如果有，在比對看看數量	
							let thisScoreDetail = await firm.getDetails();
							
							if( !thisScoreDetail ){
								FirmScoreDetail.create({
									firm_id : firm.get("id"),
									pay_back : req.body.pay_back
								});
								return { answer : true, message : `「${firm.get("po_number")}」的「${firm.get("item_id")}」已補回被退貨數量` };
							}else{
								return { answer : false, message : "已經有存在退貨補回記錄" };
							}
						}else{
							return { answer : false, message : "退貨補回數空白或超過退貨數量" };
						}
                        // let updateRow = {};
						// for( let key in req.body ){
                        //     if( firm.get(key) !== req.body[key] && req.body[key] ){
                        //         updateRow[key] = req.body[key];
                        //     }
                        // }
						// if( this.getObejctLength(updateRow).length >0 )FirmScore.update(updateRow,{where : where} );
						// return { answer : true, message : `採購單「${po_number}」中品號「${item_id}」評比內容更新` };
					}else{
						let createRow = {};
						for( let k in req.body ){
							createRow[k] = req.body[k];
						}
						//欄位都有填資料才新增
						if( this.getObejctLength(createRow).length >0 ){
							FirmScore.create(createRow);
                            return { answer : true, message : `採購單「${po_number}」中品號「${item_id}」評比內容已新增` };
						}else{
                            return { answer : false, message : `評比內容空白` };
                        }

					}
				});

       } else{
            result.answer = false, result.message = "品號或訂單編號為空";
       }

       res.json( result );
	}
    //先找到該廠商所有的評分項目，再抓品項相關資料
        //1.退貨狀況
        //2.補回狀況
        //3.良率
	async score(req, res){
		const { start_date, end_date } = this.getPurchaseDateRange(req);
		let result = { firm : {}, partRcord : [], start_date, end_date, chart : [] };
		//先抓廠商資料
		result.firm = await this.getFirmName(req.body.store_id);
		//收集目前廠商進貨狀況，如果沒有
		result.partRcord = await PURTC.thisModel
			.findAll({
				where : { TC004 : baseClass.contentReplaceSpace(req.body.store_id) , TC003 : { [OP.between] : [start_date, end_date] }},
				order : [["TC003","DESC"]]
			})
			.then(async data=>{
				let list = {};
				for( let k=0; k<data.length; k++ ){
					let thisPurtc = data[k].toJSON();
					let purtdSelect = { where : { TD002 : thisPurtc.TC002 } };
					
					if( req.body.partNo )purtdSelect.where.TD004 = req.body.partNo;
					await PURTD.thisModel.findOne(purtdSelect)
						.then(async detail =>{
							try {
								let purtd = ( detail )? detail.toJSON(): null;
								
								if( purtd !== null ){
									if( !list[ purtd.TD004 ] ){
										list[ purtd.TD004 ] = {
											name : purtd.TD005,
											spec : purtd.TD006,
											Purchase : {}
										};
									}
									try{
										purtd.TC003 = thisPurtc.TC003;
										list[ purtd.TD004 ].Purchase[purtd.TD002] = purtd;
										let Score = await FirmScore
										.findOne({ where : { po_number : baseClass.contentReplaceSpace(purtd.TD002), item_id : baseClass.contentReplaceSpace(purtd.TD004) } })
										.then(async score=>{
											let thisScore = (score) ?score.toJSON() : null;
											if( thisScore ){
												thisScore.Back = await score.getDetails();
											}
											return thisScore;
										});
										list[ purtd.TD004 ].Purchase[purtd.TD002].Score = Score;
									}catch(err){}
								}

							} catch (error) { }
						});
				}
				return list;
			});
		//計算到貨達成率
		for( let no in result.partRcord ){
			let thisPart = result.partRcord[no];
			let totalBuy = 0, totalGet = 0;
			
			for( let item in thisPart.Purchase ){
				let thisPurchase = thisPart.Purchase[item];
				
				totalBuy += parseInt(thisPurchase.TD008);
				totalGet += parseInt(thisPurchase.TD015);
			}
			
			result.partRcord[no].totalBuy = totalBuy;
			result.partRcord[no].totalGet = totalGet;
			result.partRcord[no].getParsan = (( totalGet / totalBuy )*100).toFixed(2);//算百分比後，取小數兩位
		}
		// //退貨率
		for( let no in result.partRcord ){
			let totalReturn = 0;
			for( let item in result.partRcord[no].Purchase ){
				
				let thisPurchase = result.partRcord[no].Purchase[item];//採購單明細
				let thisGet = ( thisPurchase )?parseInt(thisPurchase.TD015) : 0;//採購單收貨數量
				try{
					totalReturn += thisPurchase.Score.returns;
					result.partRcord[no].Purchase[item].returnParsan = (( (thisPurchase.Score.returns) / thisGet )*100).toFixed(2);
				}catch( err ){}
			}
			result.partRcord[no].totalReturn = totalReturn;
		}
		res.json(result);
    }
    
  	async getList(req){
      try {
          	let { date, poNo } = ( this.getObejctLength(req.body).length >0  )? req.body : req.query;

          	if( !date ){
				date = baseClass.getNowDate();
          	}
        	return await this.getPurchase({ date, poNo });
          
      } catch (err) {
          return null;
      }
   	}
  //如果沒有po number就依照某天預定交貨日抓取
  async getPurchase(obj){
      
      let select = { 
          attributes : Object.keys( dataTablePrimary.PURTD.column ),
          order : [ ["TD012", "ASC"] ]
      };

      if( obj.poNo ){
          select.where = {
              TD024 : { [OP.like] : `%${obj.poNo}%` }
          };
      }else{
          select.where = {
              TD012 : obj.date
          };
      }
      
      return await PURTD.thisModel.findAll(select).then(list=>{ return list; });
	}
	//供應商名單,如果沒有指定供應商序號則回傳所有
	async getFirmName(store_id=""){
		if( store_id ){
			return await PURMA.thisModel
				.findOne({
					where : { MA001 : store_id }
				}).then(data=>{ return data.toJSON(); });
		}else{
			return await PURMA.thisModel
			.findAll()
			.then(data =>{ return data; });
		}
	}
	getPurchaseDateRange(req){
		let {start_date, end_date} = req.body;

		//前台如果沒有選日期區間，就抓近三個月的資料
		if( !start_date && !end_date ){
			let now,last3Month = new Date();
			last3Month.setMonth(last3Month.getMonth()-3);

			start_date = last3Month;
			end_date = now;
		}
		start_date = dateformat(start_date, "isoDate").replace(/-/gi, "");
		end_date = dateformat(end_date, "isoDate").replace(/-/gi, "");
		return { start_date, end_date };
	}
	getObejctLength(obj){
		return Object.getOwnPropertyNames( obj );
	}

}
module.exports = firm;