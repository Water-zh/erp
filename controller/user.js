const sequelize = require('sequelize');
const bcrypt = require('bcrypt-nodejs');
const radomString = require('randomstring');
const dataTablePrimary = require('../dataTablePrimary');
const { ProductDesignPermission } = require("../models/prototype");
var db = require('./db');
var _seq = app.get("web_db");
var column = dataTablePrimary.USER.column;

var accountRole = new db(`CMSME`, dataTablePrimary[`CMSME`].column);
// var userModel = new db(modelName, column);
var userModel = _seq.define(
    "User", column,
    { tableName : "users", primaryKey : true }
);
var userLogModel = _seq.define(
    "UserLog",
    dataTablePrimary.UserLog.column,
    { tableName : "user_logs", primaryKey : true }
);
//有些步驟在測試時不執行

const _testState = true;
const password = (_testState)?"UDF01" : "_password";
const email = (_testState)?"UDF02" : "email"

class user {
    findUserbyAccount( account, selects ){
        return userModel.findOne( { where : {account} }, selects ).then(async (user)=>{
            try {
                user.Role = await accountRole.findOneByWhere({ ME001 : user.department }, ['ME001','ME002',"ME005"]);
                return user;
            } catch (error) { return null;}

        });
    }
    async checkUserExist(account){
        if(!account) throw new Error('account is empty');

        let user = await this.findUserbyAccount(account, ['account', 'user_name', "_password"] );

        if( user && user[password] ){
            return {
                message : '帳號已經登陸過，可以請輸入密碼登入系統',
                answer : true,
                userInfo : user
            };
        }else if( user && !user[password] ){
            return {
                message : '帳號尚未建立密碼，請先點擊「忘記密碼 / 首次登入」',
                userInfo : user,
                answer : false
            };
        }else if( !user ) {
            return {
                message : '找不到此帳號資訊，請與系統人員聯絡',
                answer : false
            };
        }
    }
    async authUser(data){
        if( !data._password || !data.account ) return { answer : false, message : "請輸入密碼或帳號" };
 
        let user = await this.findUserbyAccount(data.account, ['account', "is_active", 'user_name', "_password", "super_user", "department", "email"] );
        user = (user)?user.toJSON() : {};
        if( user && user.password === "" ){
            return{
                answer : false,
                message : "帳號尚未設定密碼，請先至「忘記密碼/首次登入」設定線上系統密碼"
            };
        }
        let bcryptPassword = bcrypt.compareSync(data._password, user._password);
        if( user && bcryptPassword ){
            if( user.is_active && !user.is_delete ){
                //預先抓取ProductDesignPermission
                user.ProductPermission = await ProductDesignPermission.findOne({ where : { account : user.id } }).then(permission=>{ return permission; });
                //記錄登入
                userLogModel.create({
                    user_id : user.id,
                    action : "login"
                })
                .catch(err=>console.log(err));
                
                return {
                    answer : true,
                    userInfo : user,
                    message : '密碼驗證成功'
                };
            }else{
                return {
                    answer : false,
                    message : "你的帳號尚未啟用，請聯絡管理員"
                }
            }

        }else if( user && !bcryptPassword ){
            return {
                answer : false,
                message : "密碼輸入錯誤"
            }
        }else{
            return {
                answer : false,
                message : '找不到此帳號資訊，請與系統人員聯絡',
            };
        }
    }
    async forgetPassword(data){
        if( !data.account || !data.email ) return { answer : false, message : "請輸入信箱或帳號，" }
        
        let user = await this.findUserbyAccount(data.account, ['account', 'user_name', "email", "check_code"] );

        let message ='';
        let answer = false;
        let _vString = radomString.generate(6);

        //測試階段，不處理，測試過正常
        if( 1 ){
            app.mailer.send('forget',{
                to : data.email,
                subject : '東駿ＡＰＰ系統信「忘記密碼驗證信」',
                verCode : _vString
                },
                (err)=>{
                    
                    if(err){     
                        console.log(err);
                        message = `忘記密碼驗證信寄送失敗`;
                    }
                }
            );
        }

        //如果原本沒有信箱就更新
        if( !user.email )userModel.update({ email : data.email }, { account : data.account });
        //update repassword code or insert
       
        userModel.update({ check_code : _vString }, { account : data.account });
        message = "已發送驗證碼至信箱，請收信並填入驗證碼欄位中";
        answer = true;
        return { message : message, answer : answer };
    }
    async resetPassword(data){
        const wrongPasswrd = ['1234','123456789','12341234','passwrod','PASSWORD','password1'];
        let answer = true;
        let message = ``;

        if( !data.account || !data._password )return{ answer : false,  message : "帳號或密碼為空白" }
        if( data._password.length < 6 )answer = false, message="密碼長度不可小於6位數";
        if( !(/^[A-Z]/).test(data._password) ) answer = false, message = '密碼開頭必須要為大寫英文字';
        if( wrongPasswrd.indexOf(data._password) > -1 ) answer = false, message = '不可以設定簡單密碼';

        let hashPassword = bcrypt.hashSync( data._password );
        if( answer ){
            await userModel.update({ _password : hashPassword }, { account : data.account }).then(()=>{
                message = "密碼已重新設定，請用新密碼登入";
            });
        }
        
        return { answer, message };
    }
    getAccountLog(user, prop){
        return new Promise((resl, reject)=>{
            try {
                userModel.findOne({account : user.account}, ['check_code']).then((user_log)=>{
                    let res = {};
                    if( !user_log )res = { answer : false, message : "此帳號未設定驗證碼，" }
                    
                    if( user_log.check_code === prop.verify ){
                        res = {
                            answer : true,
                            userInfo : user
                        }
                    }else{
                        res =  {
                            answer : false,
                            message : "輸入的驗證碼錯誤，請注意大小寫"
                        }
                    }
                    resl(res);
                });
            } catch (error) {
                reject( error );
            }
        });
    }
    async authVerify(data){
        if( !data.account || !data.verify ) return{ answer : false, message : "請輸入帳號與驗證碼" };
        let res = {};
        let user = await this.findUserbyAccount( data.account, ['account','user_name','email','check_code']);

        if( !user ) return {answer : false, message : "找不到相關帳號資訊"};
        if( !user.check_code )res = { answer : false, message : "此帳號未設定驗證碼，" };
        if( user.check_code === data.verify ){
            userModel.update({ check_code : '' }, { account : data.account });
             res = {
                 answer : true,
                 userInfo : user
             }            
        }else{
             res =  {
                 answer : false,
                 message : "輸入的驗證碼錯誤，請注意大小寫"
            }        
        }
        
        return res;
    }
    async getAllDeparments(){
        let deparment = new db(`CMSME`, dataTablePrimary.CMSME.column);
        let allDeparment = await deparment.findAllData(["ME001","ME002"]);
        return allDeparment;
    }
    async getUserDepartment(id){
        const selects = ['account', 'user_name', "deparment"];
        return await userModel.findOne( { where : {id} }, selects ).then(async (user)=>{
            try {
                let userData = user.toJSON();
                let role = await accountRole.findOneByWhere({ ME001 : user.department }, ['ME001','ME002',"ME005"]);
                userData.role = role.toJSON();
                return userData;
            } catch (error) { return null;}

        });
        // return user;
    }
    thisUserModel(){
        return userModel;
    }
    thisUserLogModel(){
        return userLogModel;
    }
}

module.exports = user;