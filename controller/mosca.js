const mosca = require("mosca");
const moscaServer = new mosca.Server({ port : 8990 });
var ips = [""];

moscaServer.on("ready", ()=>{ console.log( "mosca server running....." ); });
moscaServer.on("published",
  function(packet, client){
    try {
      switch (packet.topic) {
        case `respone_ip`:
          let clientIP = packet.payload.toString();
          if( clientIP && !ips.indexOf(clientIP) > -1  ) ips.push( clientIP );
          break;
      }      
    } catch (err) { console.log(err); }

  }
);
function sendNoitce(title, userIP){
  try {
    //不在線上的使用者就不推送通知
    for( let key in userIP ){
      if( ips.indexOf( userIP[key] ) > 0 ){
        moscaServer.publish({
          topic : `erp/${userIP[key]}`,
          payload : title,
        }); 
      }
    }
 
  } catch (err) { console.log( `mosca.js ---> sendNotice--->error ${err}` ); }

}
exports.sendNotice = sendNoitce;
exports.moscaServer = moscaServer;