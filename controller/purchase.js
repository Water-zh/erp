const db = require('./db');
const Sequelize = require('sequelize');
const dateformat = require('dateformat');
const OP = Sequelize.Op;
const dataTablePrimary = require('../dataTablePrimary');
const PURTC = `PURTC`;
const PURTD = `PURTD`;
const purchase = app.get("PURTD");
const purchase_head = new db(PURTC, dataTablePrimary[PURTC].column);

const PurtdTableName = ( app.get("isProduct") )? PURTD : PURTD.toLocaleLowerCase()+"s";

class Purchase{
    async getPartPurchaseList(data){

        let colCount = Object.keys(dataTablePrimary[PURTD].column).length;
        //抓取零部件今天日期以後的採購紀錄
        // let now = dateformat(new Date(), "isoDate").replace(/-/gi, '');
        let now = new Date();
        // let purchase = new db(PURTD, dataTablePrimary[PURTD].column );

        return await purchase.thisModel
        .findAll({
            attributes : Object.keys(dataTablePrimary[PURTD].column),
            where: {
                TD004 : data.partNo,
                TD012 : "",
                // [OP.or] : {
                //     $or : {
                //         TD002 : { $gt : data.orderNo },
                //         TD012 : ""
                //     }
                // },
                TD012 : { [OP.like] : `%${now.getFullYear()}%` },
            },
            limit : 15,
            order : [ ["TD002", "ASC"] ]
        }).then(async purchaseList=>{
            if( purchaseList.length === 0  ) return { answer : false, message : `品號 ${data.partNo}目前沒有新的採購紀錄` } ;
            for( let k=0; k< purchaseList.length; k++ ){
                let thisRow = purchaseList[k];
                //找出此採購單的擋頭
                let purchaseHead = await purchase_head.findOneByWhere({ TC002 : thisRow.TD002 },['TC003']);
                purchaseList[k].HEAD = purchaseHead;
            }
            return { answer : true, data : purchaseList };

        });
        //開發環境用
        // if( app.get("isProduct") ){

        // }else{
        //     //正式主機用，因為mssql 2008的關係所以沒辦法直接用sequlia的query bulid
        //     console.log("is on Production purchase");
        //     let select = Object.keys(dataTablePrimary[PURTD].column).join(",");
        //     let query =`
        //         SELECT TOP 15 ${select}
        //         FROM ${PURTD}
        //         WHERE TD004 = ${data.partNo} AND  TD012 = ''
        //         ORDER BY TD002 ASC
        //     `;
        //     return await purchase._seq
        //         .query(query,  { type: Sequelize.QueryTypes.SELECT})
        //         .then(async purchaseList=>{
        //             if( purchaseList.length === 0  ) return { answer : false, message : `品號 ${data.partNo}目前沒有新的採購紀錄` } ;
        //             for( let k=0; k< purchaseList.length; k++ ){
        //                 let thisRow = purchaseList[k];
        //                 //找出此採購單的擋頭
        //                 let purchaseHead = await purchase_head.findOneByWhere({ TC002 : thisRow.TD002 },['TC003']);
        //                 purchaseList[k].HEAD = purchaseHead;
        //             }
        //             return { answer : true, data : purchaseList };
        //         });
        // }

    }
    async setStockIncomeDate(data){
        //先找到該張採購單
        let purchase = new db(PURTD, dataTablePrimary[PURTD].column );
        let thisPurchase = await purchase.findOneByWhere({ TD002 : data.purchaseNo, TD004 : data.partNo }, ['TD012']);

        if( !thisPurchase ) return{ answer : false, message : `採購單號「${data.purchaseNo}」找不到相關採購紀錄` };
        //看看此張訂購單是不是已經有交貨日期
        if( thisPurchase.TD012 ){
            return { answer : false, message : `採購單號「${data.purchaseNo}」已有填寫交貨日期，` };
        }else{
            return purchase.update({ TD012 : data.incomeDate }, { TD002 : data.purchaseNo, TD004 : data.partNo }).then(()=>{
                return { answer : true, message : `採購單號「${data.purchaseNo}」已儲存交貨日期` };
            });
        }
    }
    async getListByPO(req){

        let poNo = ( req.query.poNo )?req.query.poNo: req.body.poNo;

        if( poNo){
            poNo = poNo.split(",");
            let dataList = [];
            for(let k=0;k<poNo.length;k++){
                await purchase.thisModel
                .findAll({
                    attributes : Object.keys(dataTablePrimary[PURTD].column),
                    where : {
                        TD024 : { [OP.like] : `%${poNo[k]}%` }
                    }
                })
                .then( async data=>{
                    for( let k=0; k< data.length; k++ ){
                        let thisRow = data[k];
                        //找出此採購單的擋頭
                        // thisRow.HEAD = await purchase_head.findOneByWhere({ TC002 : thisRow.TD002 },['TC003']);
                        dataList.push(thisRow);
                    }
                });
            }
            if(dataList.length === 0){
                return { answer : false, message : `此「${poNo}」沒有採購明細` }
            }else{
                return{ answer : true, dataList }
            }
        }else{
            return { answer : false, message: '訂單編號為空' }
        }
    }
}

module.exports = Purchase;
