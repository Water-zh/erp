const db = require('./db');
const Sequelize = require('sequelize');
const dateformat = require('dateformat');
const OP = Sequelize.Op;
const purchaseClass = require("./purchase");
const dataTablePrimary = require('../dataTablePrimary');

var purchase = new purchaseClass();
var plants = app.get('MOCTA');
var plantsDetail = new db(`MOCTB`, dataTablePrimary[`MOCTB`].column);
// let tableName = ( app.get("isProduct") )? "MOCTA" : "moctas"
var plantShowPerPage = 15;
class plant {
    //web
    async index(req, res){
        if( !req.query.order_status ) req.query.order_status = 1;//預設選擇未完成工單

        let _csrf = req.csrfToken();
        let result = await this.getList(req);

        result.purchase = await purchase.getListByPO(req);
        result.pageCount = Math.ceil(result.dataCount/plantShowPerPage);
        result.nowPage = req.query.page;
        result.order_status = req.query.order_status;

        res.render(`plant/index`, {result, _csrf, plantShowPerPage});
    }

    //api
    async getList(req, res){
        let page = ( req.query.page )?req.query.page : 1;
        let result = {};
        let poNo = ( req.query.poNo )?req.query.poNo: req.body.poNo;
        let order_status = ( req.query.order_status )?req.query.order_status: req.body.order_status;

        if( poNo ){
            result = await this.getPlantListByPO({ poNo, page, order_status });
        }else{
            let start_date = ( req.query.start_date )?req.query.start_date : req.body.start_date;
            let end_date = ( req.query.end_date )?req.query.end_date : req.body.end_date;
            result = await this.getPlantListByDate({ start_date , end_date, page,  order_status });
        }
        return result
        // res.json(result);
    }
    async getPlantListByWhere(fromWehre, selectCol){
        let data = await plants.findOneByWhere(fromWehre, selectCol);
        if( !data )return { answer : false, message : "找不到相關工單紀錄" }
        return { answer : true, result : data }
    }

    async getPlantListByDate(data){
        let start_date = data.start_date;
        let end_date = data.end_date;

        //如果兩個都是空的
        if( !start_date && !end_date  ){
            let date = new Date();
            let otherDate = new Date();

            otherDate.setDate( otherDate.getDate() + 30 );//預設起始後的一個月

            let day = ( date.getDate().toString().length === 1 )? `0${date.getDate()}`: date.getDate();
            let otherDay = ( otherDate.getDate().toString().length === 1 )? `0${otherDate.getDate()}`: otherDate.getDate();
            let startMonth = ( (date.getMonth()+1).length == 1 )? `0${date.getMonth()+1}`: date.getMonth()+1;
            let otherMonth = ( (otherDate.getMonth()+1).length == 1 )? `0${otherDate.getMonth()+1}`: otherDate.getMonth()+1;

            if( !start_date ) start_date = `${date.getFullYear()}${startMonth}${day}`;
            if( !end_date ) end_date = `${otherDate.getFullYear()}${otherMonth}${otherDay}`;
        }else{
            start_date = dateformat(start_date, 'isoDate').replace(/-/gi,'');
            end_date = dateformat(end_date, 'isoDate').replace(/-/gi,'');
        }


        //依「start_date」到「end_start」抓取預計開工的工單，照近～遠的日子排序
        let selectCol = `TA001,TA002,TA003,TA004,TA005,TA006,TA007,TA009,TA010,TA012,TA015,TA016,TA017,TA018,TA033,TA034,TA035`;
        let result = {};
        let selectWhere = {
            TA009 : { [OP.gt] : start_date },
            [OP.and] : { TA009 : { [OP.lt]: end_date } },
            TA015 : { [OP.ne] : plants._seq.col("TA016") }
        };
        if( data.order_status ) selectWhere.TA011 = data.order_status.toString();

        result = await plants.thisModel
            .findAll({
                where : selectWhere,
                attributes : selectCol.split(","),
                offset : ( data.page -1 )* plantShowPerPage,
                limit : plantShowPerPage
            })
            .then(async list=>{
                if( list.length === 0 ) return { answer : false , data : [], message : `${start_date} ~ ${end_date} 沒有工單資料` }
                let datas = [];
                for( let v = 0; v< list.length ; v++ ){
                    let thisRow = list[v];
                    if( thisRow.TA015 !== thisRow.TA016 || thisRow.TA016 === 0 ){
                        datas.push( thisRow );
                    }
                }
                return { answer : true, data : list, message : `${start_date} ~ ${end_date} 工單資料` }
            });

        result.dataCount = await plants.thisModel
            .count({
                where : selectWhere,
            })
            .then(count => { return count; });
        //正式環境因為ms sql是2008版本，所以必須使用舊式sql指令
        // if( app.get("isProduct") ){
        //
        //     let notInTOP = (data.page-1)*plantShowPerPage;
        //     let query = `SELECT TOP ${plantShowPerPage} ${selectCol}
        //         FROM MOCTA
        //         WHERE TA002 NOT IN (
        //             SELECT TOP ${notInTOP} TA002 FROM MOCTA ORDER BY TA009 ASC
        //         ) AND TA009 > ${start_date} AND TA009 < ${end_date} AND TA015 != TA016
        //         ORDER BY TA009 ASC
        //     `;
        //     let countQuery = `SELECT TA002
        //         FROM MOCTA
        //         WHERE TA009 > ${start_date} AND TA009 < ${end_date} AND TA015 != TA016
        //         ORDER BY TA009 ASC
        //     `;
        //     result = await plants._seq
        //         .query(query, {type : Sequelize.QueryTypes.SELECT})
        //         .then(async list=>{
        //             if( list.length === 0 ) return { answer : false , data : [], message : `${start_date} ~ ${end_date} 沒有工單資料` }
        //             let datas = [];
        //             for( let v = 0; v< list.length ; v++ ){
        //                 let thisRow = list[v];
        //                 if( thisRow.TA015 !== thisRow.TA016 || thisRow.TA016 === 0 ){
        //                     datas.push( thisRow );
        //                 }
        //             }
        //             return { answer : true, data : list, message : `${start_date} ~ ${end_date} 工單資料` }
        //         });
        //         result.dataCount = await plants._seq
        //         .query(countQuery, {type : Sequelize.QueryTypes.SELECT})
        //         .then(async list=>{ return list.length });
        // }else{
        //
        // }
        return result;
    }
    //2017/8/24 調整可以接受多個訂單編號-> `170368,170366`
    async getPlantListByPO(data){
        let poNo = data.poNo.split(",");
        if( poNo.length === 0 )return { answer : false, message : "尚未傳入訂單編號" };

        let selectCol = `TA001,TA002,TA003,TA004,TA005,TA006,TA007,TA009,TA010,TA012,TA015,TA016,TA017,TA018,TA033,TA034,TA035`;
        let list = [];
        let dataCount = 0;
        let pageIndex =  data.page-1;

        for( let k =0; k < poNo.length; k++ ){
            let selectWhere = {
                TA033 : { [OP.like] : `%${poNo[k]}%` }
            };
            if( data.order_status && data.order_status === "1" ) selectWhere.TA011 = data.order_status;

            await plants.thisModel
            .findAll({
                attributes : selectCol.split(","),
                where : selectWhere,
                order : [ ["TA009", "ASC"] ],
                offset : ( data.page -1 )* plantShowPerPage,
                limit : plantShowPerPage
            })
            .then(async data =>{
                for(let j=0; j<data.length; j++){
                    await list.push(data[j]);
                }
            });
            dataCount += await plants.thisModel
            .count({
                where : {
                    TA033 : { [OP.like] : `%${poNo[k]}%` }
                },
            })
            .then( count =>{ return count } );
            // if( app.get("isProduct") ){
            //     let notInTOP = (data.page-1)*plantShowPerPage;
            //     let query = `
            //         SELECT TOP ${plantShowPerPage} ${selectCol}
            //         FROM MOCTA
            //         WHERE TA002 NOT IN (
            //             SELECT TOP ${notInTOP} TA002 FROM MOCTA ORDER BY TA009 ASC
            //         )
            //         AND TA033 LIKE '%${poNo[k]}%'
            //         ORDER BY TA009 ASC
            //     `;
            //     let countQuery = `
            //         SELECT TA002,TA009,TA033
            //         FROM MOCTA
            //         WHERE TA033 LIKE '%${poNo[k]}%'
            //         ORDER BY TA009 ASC
            //     `;
            //     await plants._seq
            //         .query(query, {type : Sequelize.QueryTypes.SELECT})
            //         .then(async data=>{
            //             for(let j=0; j<data.length; j++){
            //                 await list.push(data[j]);
            //             }
            //         });
            //     plants._seq
            //         .query(countQuery, {type : Sequelize.QueryTypes.SELECT})
            //         .then(count=>{ return count.length });
            // }else{
            //
            // }

        }
        if( list.length > 0 ){
            return { answer : true, data : list, message : `訂單編號 ${data.poNo} 相關工單`, dataCount }
        }else{
            return{ answer : false, message : `訂單編號 ${data.poNo} 沒有查詢到相關記錄` }
        }
    }
    async getPlantDetail(data){
        if( !data.orderNo )return { answer : false, message : "尚未傳入工單編號" };
        let moctaSelect = [];

        for(let key in dataTablePrimary[`MOCTA`].column){
            moctaSelect.push(key);
        }

        let plant = await this.getPlantListByWhere({ TA002 : data.orderNo }, moctaSelect);
        if( plant.answer ) {
            let selectCol = [];
            let detailData = [];

            for (let key in dataTablePrimary[`MOCTB`].column) {
                selectCol.push(key);
            }

            let details = await plantsDetail.findAllByWhere({TB002: data.orderNo}, selectCol);
            if (!details) plant.message = `${data.orderNo} 找不到工單詳細資料`;

            //有缺料的才顯示
            for( let v=0; v < details.length; v++ ) {
                let thisDetail = details[v];
                if (thisDetail.TB016 === 0 || thisDetail.TB004 !== thisDetail.TB005) {
                    detailData.push(thisDetail);
                }

            }
            plant.detail = detailData;
            plant.message = ( detailData.length ===0 )? `此工單沒有缺件`: '';
            return plant;
        }else{
            return plant;
        }
    }
    async getPlantPartDetail(data) {
        //先抓這一個工單中的組件
        let selectCol = [];
        let selectDetail = [];

        let partModelName = `INVMB`;
        let partDetailModelName = `INVMC`;
        let part = new db(partModelName, dataTablePrimary[partModelName].column);
        let partDetail = new db(partDetailModelName, dataTablePrimary[partDetailModelName].column);

        for (let key in dataTablePrimary[partModelName].column) {
            selectCol.push(key);
        }

        let partData = await part.findOneByWhere({MB001: data.partNo}, selectCol);
        if (!partData)return {answer: false, message: `找不到品號：${data.partNo} 相關資料`};

        //再抓組建相關的採購紀錄 和 庫存數量
        for (let key in dataTablePrimary[partDetailModelName].column) {
            selectDetail.push(key);
        }
        let partDetailData = await partDetail.findOneByWhere({ MC001: data.partNo }, selectDetail);

        return{
            answer : true,
            part : partData,
            detail : partDetailData,
            message : ( !partDetailData )? `品號${data.partNo} 找不到詳細資料` : ''
        };
    }
    async getPartOtherOrder(data){
        //找出其他用到這一個品號的其他訂單
        var datas = [];

        let now = dateformat(new Date(), "isoDate").replace(/-/gi, "");
        let selectDay = new Date();
        selectDay.setDate(selectDay.getMonth()-1);
        selectDay = dateformat(selectDay, "isoDate").replace(/-/gi, "")

        return await plants.thisModel
        .findAll({
            attributes : Object.keys(dataTablePrimary['MOCTA'].column),
            where: {
                [OP.and] : {
                    TA006 : data.partNo,
                    TA002 : { [OP.ne]: data.orderNo },
                    TA009 : { [OP.gte] : selectDay }
                }
            },
            limit : 15,
            order : [ ["TA009", "ASC"] ]
        })
        .then(async orther=>{
            //因為是用品號找的紀錄，所以一定會有本身這一比
            //所以去除本身這一筆後再來看看是不是有其他結果
            for( let v=0; v<orther.length ; v++ ){
                var thisRow = orther[v];
                //TA015 預計領料數量
                //TA016 已領料數量

                if( thisRow.TA015 !== thisRow.TA016 || thisRow.TA016 === 0 ){
                    datas.push(thisRow);
                }
            }

            if( datas.length === 0 )return{ answer : false, message : `除了目前工單${data.orderNo}, 沒有其他工單使用這個組件`}
            return { answer : true, data : datas };
        });
        // if( app.get("isProduct") ){
        //     let select = Object.keys(dataTablePrimary['MOCTA'].column).join(",");
        //     let query = `
        //         SELECT TOP 15 ${select}
        //         FROM  MOCTA
        //         WHERE TA002 != ${data.orderNo} AND TA006 = ${data.partNo} AND TA009 >= ${selectDay}
        //         ORDER BY TA009 ASC
        //     `;
        //     return await plants._seq
        //         .query(query, { type: Sequelize.QueryTypes.SELECT})
        //         .then(async orther=>{
        //             //因為是用品號找的紀錄，所以一定會有本身這一比
        //             //所以去除本身這一筆後再來看看是不是有其他結果
        //             for( let v=0; v<orther.length ; v++ ){
        //                 var thisRow = orther[v];
        //                 //TA015 預計領料數量
        //                 //TA016 已領料數量

        //                 if( thisRow.TA015 !== thisRow.TA016 || thisRow.TA016 === 0 ){
        //                     datas.push(thisRow);
        //                 }
        //             }

        //             if( datas.length === 0 )return{ answer : false, message : `除了目前工單${data.orderNo}, 沒有其他工單使用這個組件`}
        //             return { answer : true, data : datas };
        //         });
        // }else{


        // }

    }
}

module.exports = plant;
