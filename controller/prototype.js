const { ProductDesign, ProductDesignCategory, ProductDesignPhoto, ProductDesignStatusChange, ProductDesignPermission, ProductDesignMessage } = require("../models/prototype");
const userClass = require("./user");
const fs = require("fs");
const imageSize = require('image-size');
const path = require("path");
const baseClass = require("./base");
const Sequelize = require('sequelize');
const OP = Sequelize.Op;
const base = new baseClass();
const prototypeStatus = ["","未確認","不要","打樣","待討論"];
const prototypeStatusColor = ["","","red","green","blue"];
const user = new userClass();
const productDir = `designs`;

//判斷使用者權限移到 route/sysytem.js, 但 還是使用此class下的method
class prototype{
  async index(req, res){
		let result = { product : [], _token : req.csrfToken(), prototypeStatus, designers : await this.getDesingerAccount(), permission : await this.checkPermission(req.session.userInfo)};
		if( result.permission.can_see ){
			result.product = await ProductDesign
			.findAll({ attributes : ["id","owner","status","title"] })
			.then(async data=>{
				try {
					for(let key in data){
						data[key].statusBtns = this.setStatusBtns(data[key]);
						data[key].Image = await data[key].getPhotos();
					}
				} catch (err) {}
				return data;
			});  
			res.render("prototype/index",result);
		}else{
			res.send("你沒有權限可以使用這個功能，請洽管理員。");
		}
  }
  async create(req, res){
    	let result = { category : [], _token : req.csrfToken(), permission : await this.checkPermission(req.session.userInfo)};
		if( result.permission.can_see && result.permission.is_designer ){
			res.render("prototype/create", result);
		}else{
			res.redirect("/system/prototype");
		}
  }
  async store(req, res){
		let result = { answer : false ,errMessage : ""};
		let permission = await this.checkPermission(req.session.userInfo);
		//是設計師帳號而且可以看到這功能的才能新增
		if( permission.is_designer && permission.can_see ){
			if( req.body.title && req.files ){
				const {title, slogan, keyword} = req.body;
				let photoes = [];//上傳並收集檔名
				let now = new Date();
				let thisProductDIR = `${title}_${now.getFullYear()}-${now.getMonth()+1}-${now.getDay()}`;
				let setProductDIR = `${productDir}/${thisProductDIR}`;
				//先建立產品專屬資料夾
				fs.exists(`./uploads/${setProductDIR}`,(isExists)=>{
					if( !isExists ){
						fs.mkdir(`./uploads/${setProductDIR}`,(err)=>{ console.log(err); });
					}
				});
				
				for(let key in req.files.photoes){
					let thisFileName = await base.uploadFile(req.files.photoes[key], setProductDIR);
					photoes.push( `${thisProductDIR}/${thisFileName}` );
				}
				//新增此筆樣式內容
				ProductDesign.create({ title ,slogan ,keyword, file_path: thisProductDIR, owner : req.session.userInfo.id })
					.then(newProduct=>{
						for( let k in photoes ){
							ProductDesignPhoto.create({ proto_id : newProduct.null, image : photoes[k] });
						}
					});
				result.answer = true;
				result.errMessage = "已經上傳";
			}else{
				result.errMessage = "「設計圖檔」沒有上傳 或 標題空白！";
			}
		}else{ result.errMessage ="你沒有權限可以新增產品設計。請洽管理員"; }
    res.json(result);
  }
	async show(req, res){
		const {id} = req.params;
		const permission = await this.checkPermission(req.session.userInfo);
		let result = { product:{}, _token : req.csrfToken() };
		result.product = await ProductDesign.findOne({ where : {id} })
			.then(async data=>{ 
				let product = data.toJSON(), photo = {};
				try {
					photo = await data.getPhotos();	
				} catch (error) { }
				
				product.Photoes = photo;
				return product;
			});
		if( permission.account === result.product.owner  ){
			res.render("prototype/show",result);
		}else{
			res.send("非此筆產品設計擁有者無法編輯，請聯絡管理員。");
		}
  }
	async update(req, res){
		const { id , title, keyword } = req.body;
		let updateRow = {}, result = { message : "", answer : false };
		let product = await ProductDesign.findOne({ where : { id }, attributes : ["id","file_path","title","keyword"] }).then(data=>{ return data });
		delete req.body.id; delete req.body._csrf;
		//先改變上傳檔名
		//再把原本的圖片記錄移除，舊圖片不刪
		if( req.files.photoes ){
			let now = new Date(), photoes = [];
			for( let key in req.files.photoes ){
				let orName = req.files.photoes[key].originalname;
				let sOrName = orName.split(".");
				let thisFileName = `${sOrName[0]}_${now.getFullYear()}-${now.getMonth()+1}-${now.getDay()}.${sOrName[1]}`;
				req.files.photoes[key].originalname = thisFileName;
				base.uploadFile(req.files.photoes[key], `${productDir}/${product.file_path}`);
				photoes.push((product.file_path)?`${product.file_path}/${thisFileName}` : thisFileName);
			}
			if( photoes.length > 0 ){
				//刪除原本的圖片記錄
				ProductDesignPhoto.destroy({ where: { proto_id : id } });
				for( let k in photoes ){
					ProductDesignPhoto.create({ proto_id : id, image : photoes[k] });
				}
				result.answer = true;
			}
		}else{ result.message = "未更新產品圖片\n"; }
		//確認需要更新內容
		for(let key in req.body){
			if( req.body[key] != product[key] ){
				updateRow[key] = req.body[key];
			}
		}
		if( Object.getOwnPropertyNames( updateRow ).length > 0 ){
			ProductDesign.update(updateRow, { where : { id } });
			result.answer = true;
		}else{ result.message += "未更新標題及關鍵字\n"; }		
		res.json(result);
	}
	async search(req, res){
		let result = { answer : false , message : "", html :"" };
		let products = await this.searchProduct(req.body);
		let permission = await this.checkPermission(req.session.userInfo);
		for( let key in products ){
			let row = products[key];
			let link = ``;
			let image = (row.Image[0])? `/static/${productDir}/${row.Image[0].image}`:"";
			if( row.owner == permission.account ){
				link = `
					<h3>
						<a href="/system/prototype/show/${row.id}">${row.title}</a>
					</h3>
				`;
			}else{
				link = `<h3>${row.title}</h3>`;
			}
			result.html += `
				<div class="col-sm-12 col-md-4" style="min-height: 375px;">
					<div class="thumbnail">
						<a id="showAll" href="javascript:;" data-id="${row.id}">
							<div style="width: 100%; height: 280px; background-image:url(${image}); background-repeat:no-repeat;"></div>
							//<img src="${image}" alt="" style="width: 100%; height: 200px;">
						</a>
						<div class="caption">
							${link}
							<p>${row.slogan}</p>
							<p>${this.setStatusBtns(row)}</p>
						</div>
					</div>
				</div>
			`;
		}
		res.json(result);
	}
	//api
  async showProductImages(req, res){
		const {proto_id} = req.body;
		let result = { product : {} };
		result.product = await ProductDesign.findOne({ where : { id : proto_id } })
			.then(async data =>{
				let product = data.toJSON();
				try {
				    let photoes = [];
				    let rawPhoto = await data.getPhotos();
				    for( let key in rawPhoto ){
						//擷取圖片width and height
						let thisSize = imageSize(`./uploads/${productDir}/${rawPhoto[key].image}`);
				      	photoes.push({ src : `/static/${productDir}/${rawPhoto[key].image}`, w : thisSize.width, h: thisSize.height});
					}
            product.Photoes = photoes;
				} catch (err) { console.log( err ); }
				return product;
			});
		res.json(result);
	}
	async sendStatus(req, res){
		const {id, status} = req.body;
		const { userInfo } = req.session;
		let result = { type : "", message : "" };
		//先即時檢查此使用者是否記錄可以改狀態
		let permission = await this.checkPermission(userInfo);
		if( permission.can_answer){
			if( id && status ){
				ProductDesign.update({status}, { where : { id } }).catch(err=>{ console.log(err); });
				ProductDesignStatusChange.create({ proto_id : id, changer : parseInt(req.session.userInfo.id), status });
				result.type = "info";
				result.message ="此樣式已回覆";
			}else{
				result.type = "error";
				result.message = "未接收到id序號或回覆代號";
			}
		}else{
			result.type = "error";
			result.message = "你沒有權限可以回應";
		}

		res.json(result);
	}
	async getMessages(req, res){
		const id = req.body.id;
		const {userInfo} = req.session;
		var result = { answer : false, message :"", datas : []};
		//先檢查是否有「回應」的權限
		if(userInfo.ProductPermission.can_answer){
			//依照帳號ID和產品ID抓取歷史紀錄
			result.datas = await ProductDesignMessage.findAll({ where: { proto_id : id } })
				.then(async data=>{
					let rowData = [];
					if( data ){
						let userModel = user.thisUserModel();
						for (let v = 0; v < data.length; v++) {
							const row = data[v].toJSON();
							row.User = await userModel.findOne({ where:{ id : row.account } }).then(user=>{ return user; });
							rowData.push(row);
						}
					}
					return rowData;
				});
			if( result.datas.length > 0 ){
				result.answer = true;	
			}
		}else{
			result.message = "你沒有權限可以回應產品討論";
		}
		
		res.json(result);
	}
	async setMessages(req, res){
		const {userInfo} = req.session;
		const { id, content } = req.body;
		var result = { answer : false, message : "", userName : userInfo.user_name};
		if(id !== '' && content !== ""){
			//先儲存留言內容
			await ProductDesignMessage.create({proto_id : id, account : userInfo.id, message : content})
			.then(async newMessage=>{
				if( newMessage ){
					result.answer = await true;
					result.message = await "留言已儲存。";
				}else{
					result.message = await "留言儲存失敗。";
				}
			});
		}else{
			result.message = "回應內容空白或沒有回傳序號。"
		}
		res.json(result);
	}
	//static method 
	async searchProduct(search){
		const {keyword, desinger} = search;
		let select = { where : {} };
		if( keyword !== '' ){
			select.where.keyword = { [OP.like] : `%${keyword},%` };
		}
		if( desinger !== '' && desinger !== 0 ){
			select.where.owner = parseInt(desinger);
		}
		let products = await ProductDesign.findAll(select)
			.then(async data=>{
				let row = [];
				for( let k=0; k<data.length; k++ ){
					let thisRow = data[k].toJSON();
					thisRow.Image = await data[k].getPhotos();
					row.push(thisRow);
				}
				return row;
			});
		return products;
	};
	setStatusBtns(data){
		var statusBtns = ``;//產生狀態按鈕，如果已經有選擇回應，該回應點亮顏色
		for(let ck in prototypeStatusColor){
			if( prototypeStatusColor[ck] !== "" ){
				let statusColor = ( data.status == ck )? prototypeStatusColor[ck] : "default";
				statusBtns += `<a href="javascript:;" data-id="${data.id}" data-type="${ck}" class="btn ${statusColor} actionAnswer">${prototypeStatus[ck]}</a>`;
			}
		}
		return statusBtns;
	}
	//
	async getDesingerAccount (){
		//獲得所有設計師的帳號
		return await ProductDesignPermission.findAll({ where : { is_designer : 1 } })
			.then(async data=>{
				let id = [];
				let userModel = user.thisUserModel();
				for(let key in data ){
					id.push(data[key].account);
				}
				return await userModel.findAll({ where : {id} })
					.then(userData=>{
						return userData;
					});
			});
	}
	async checkPermission(userInfo){
		//特定帳號 才可以進功能 
		//設計師才可以上傳產品設計
		let result = {account : "" , is_designer : false, can_see : false, can_answer :false };
		await ProductDesignPermission.findOne({ where : {account: userInfo.id} })
			.then(data=>{
				try {
					let permission = data.toJSON();
					for( let key in result ){
						if( key === "account" ){
							result[key] = ( permission[key] )? permission[key] : "";
						}else{
							result[key] = ( permission[key] )? true : false;
						}
						
					}
				} catch (err) {}
			});
		return result;
	}
}

module.exports = prototype;