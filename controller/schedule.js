const Schedule = require('node-schedule');
const dateformat = require("dateformat");

const db = require('./db');
const dataPrimary = require("../dataTablePrimary");
const MOCTA = app.get('MOCTA');
/**
    *    *    *    *    *    *
    ┬    ┬    ┬    ┬    ┬    ┬
    │    │    │    │    │    |
    │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
    │    │    │    │    └───── month (1 - 12)
    │    │    │    └────────── day of month (1 - 31)
    │    │    └─────────────── hour (0 - 23)
    │    └──────────────────── minute (0 - 59)
    └───────────────────────── second (0 - 59, OPTIONAL)
 */
Schedule.scheduleJob("0 30 8 * * 1-6", async ()=>{
    // 檢查3~5天後的工單是否有缺件 

    let rang = ["3","4","5"]; //今天往後推Ｘ天後
    let daysStock = "";

    for (var k = 0; k < rang.length; k++) {
        let today = new Date(2017,2,11);
        let addDay = rang[k];
        let checkDay = dateformat( today.setDate( today.getDate()+parseInt(addDay) ), "isoDate");
        let plantCount = await getDataFromSql(checkDay);

        if( plantCount !== 0 ){
            daysStock += `${checkDay} 有${plantCount} 張工單缺料 \/n`;
        }
    
    }
    // 推播
    console.log(daysStock);
});

let getDataFromSql = async function (date){
    let selectCol = `TA001, TA002, TA003, TA004, TA005, TA006, TA007, TA009, TA010, TA012, TA015, TA016, TA017, TA018, TA034, TA035`;
    let checkDay = date.replace(/-/gi, "");

    let query = `SELECT COUNT(TA001) as count FROM MOCTA 
                    WHERE TA009 = "${checkDay}" AND TA015 != TA016
                    ORDER BY TA009 ASC`;

    return await MOCTA._seq.query(query, { type: MOCTA._seq.QueryTypes.SELECT}).then(data=>{
        return data[0].count;
    });

}
