# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.19-MariaDB)
# Database: SIGMA
# Generation Time: 2017-10-12 06:32:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table connection_changes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `connection_changes`;

CREATE TABLE `connection_changes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '序號',
  `connection_id` int(11) DEFAULT NULL COMMENT '[fk]所屬聯絡單',
  `c_department` varchar(10) DEFAULT NULL COMMENT '更改聯絡單的單位',
  `changer` varchar(10) DEFAULT NULL COMMENT '更改帳號',
  `column` varchar(20) DEFAULT NULL COMMENT '更改欄位',
  `status` varchar(5) DEFAULT NULL COMMENT '新增或減少',
  `record` text COMMENT '修改內容',
  `updatedAt` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table connection_departments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `connection_departments`;

CREATE TABLE `connection_departments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `connection_id` int(11) DEFAULT NULL COMMENT '[fk]所屬聯絡單',
  `department` varchar(10) DEFAULT NULL COMMENT '對象部門',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新日',
  `createdAt` datetime DEFAULT NULL COMMENT '建立日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table connection_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `connection_logs`;

CREATE TABLE `connection_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '[fk]所屬帳號',
  `connection_id` int(11) NOT NULL COMMENT '[fk]所屬聯絡單',
  `action` varchar(15) NOT NULL DEFAULT '' COMMENT '動作',
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table connection_records
# ------------------------------------------------------------

DROP TABLE IF EXISTS `connection_records`;

CREATE TABLE `connection_records` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `connection_id` int(11) DEFAULT NULL COMMENT '[fk]所屬聯絡單',
  `user` varchar(10) DEFAULT NULL COMMENT '已觀看使用者',
  `department` varchar(10) DEFAULT NULL COMMENT '部門',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新日',
  `createdAt` datetime DEFAULT NULL COMMENT '建立日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table connections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `connections`;

CREATE TABLE `connections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '序號',
  `is_delete` int(1) DEFAULT '0' COMMENT '是否已刪除',
  `owner` varchar(8) DEFAULT NULL COMMENT '擁有者',
  `owner_department` varchar(8) DEFAULT NULL COMMENT '擁有者部門',
  `title` varchar(25) DEFAULT '' COMMENT '聯絡單標題',
  `content` text COMMENT '聯絡單內容',
  `file` varchar(150) DEFAULT NULL COMMENT '附加檔案',
  `remind_date` date DEFAULT NULL COMMENT '提醒日期',
  `keyword` varchar(100) DEFAULT NULL COMMENT '關鍵字',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新日',
  `createdAt` datetime DEFAULT NULL COMMENT '建立日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '序號',
  `is_check` int(2) NOT NULL DEFAULT '0' COMMENT '已審核',
  `owner` varchar(8) DEFAULT NULL COMMENT '擁有者',
  `owner_department` varchar(8) DEFAULT NULL COMMENT '擁有者部門',
  `title` varchar(25) DEFAULT '' COMMENT '聯絡單標題',
  `content` text COMMENT '聯絡單內容',
  `file` varchar(150) DEFAULT NULL COMMENT '附加檔案',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新日',
  `createdAt` datetime DEFAULT NULL COMMENT '建立日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table news_changes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news_changes`;

CREATE TABLE `news_changes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '序號',
  `news_id` int(11) DEFAULT NULL COMMENT '[fk]所屬聯絡單',
  `changer` varchar(10) DEFAULT NULL COMMENT '更改帳號',
  `column` varchar(10) DEFAULT NULL COMMENT '更改欄位',
  `status` varchar(5) DEFAULT NULL COMMENT '新增或減少',
  `record` text COMMENT '修改內容',
  `updatedAt` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table news_records
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news_records`;

CREATE TABLE `news_records` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL COMMENT '[fk]所屬聯絡單',
  `user` varchar(10) DEFAULT NULL COMMENT '已觀看使用者',
  `department` varchar(10) DEFAULT NULL COMMENT '部門',
  `updatedAt` datetime DEFAULT NULL COMMENT '更新日',
  `createdAt` datetime DEFAULT NULL COMMENT '建立日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_logs`;

CREATE TABLE `user_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '[fk]使用者序號',
  `action` varchar(20) NOT NULL DEFAULT '' COMMENT '動作（登入/登出）',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `is_active` int(1) NOT NULL COMMENT '帳號啟用',
  `is_delete` int(1) NOT NULL DEFAULT '0' COMMENT '已刪除的帳號',
  `super_user` int(1) NOT NULL COMMENT '超級使用者',
  `department` varchar(11) NOT NULL DEFAULT '' COMMENT '部門',
  `account` varchar(10) NOT NULL DEFAULT '' COMMENT '帳號',
  `user_name` varchar(30) NOT NULL DEFAULT '' COMMENT '使用者姓名',
  `email` varchar(150) NOT NULL DEFAULT '' COMMENT '信箱',
  `_password` varchar(200) NOT NULL DEFAULT '' COMMENT '密碼',
  `check_code` varchar(10) NOT NULL DEFAULT '' COMMENT '預留欄位',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `is_active`, `is_delete`, `super_user`, `department`, `account`, `user_name`, `email`, `_password`, `check_code`, `createdAt`, `updatedAt`)
VALUES
	(2,1,0,1,'SG','Wayne','黃光榮','b3031717@gmail.com','$2a$10$i5eUB86izyxt3sQ17mnePO.SHs6ZHKZJCLswFad5I3lX8QBxA28Ri','','2017-09-01 00:00:00','2017-09-01 00:00:00'),
	(3,1,0,0,'PMC','walter','莊勝雄','b3031717@hotmail.com','$2a$10$szknQNQkuX.PUe7E523POOGB6gy1cdAGkZp1sTNyi9/s1Bm6iNXUS','','2017-09-27 08:18:05','2017-09-27 08:42:49'),
	(4,0,0,0,'566','LG002','陳良','b3031717@yahoo.com','$2a$10$WIrAgW1pipJamcOwaQzIYO9vdfDqLU.frybva2ozDd2.H9bzE9d3K','','2017-09-27 08:58:28','2017-09-27 08:58:28');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
