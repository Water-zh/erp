(function(){
    var elements = [
         document.querySelector('button#backBtn'),
         document.querySelector('a#forget-password')
    ];
    var submit = document.querySelector("#submit");
    var clickHandle = function(){
        var _this = this;
        var showClass = '', hideClass = '';
        
        switch( _this.id ){
            case 'forget-password':
                showClass = 'forget-form', hideClass = 'login-form';
                break;
            case 'backBtn':
                showClass = 'login-form', hideClass = 'forget-form';
                break;
        }
        
        document.querySelector(`form.${showClass}`).style.display = 'block';
        document.querySelector(`form.${hideClass}`).style.display = 'none';  
    };

    for(key in elements){
        if( elements[key] ){
            elements[key].addEventListener('click', clickHandle);
        }
    }
    var eachInputs = function(inputs, select){
        var canSubmit = true;
        var datas = [];
        var selectData = "";

        for(var k=0; k<inputs.length; k++){
            var thisInput = inputs[k];
            if( thisInput.value ==='' ){
                thisInput.placeholder = "必填欄位";
                canSubmit = false;
            }else{
                if( thisInput.name == select ) selectData=thisInput.value;
                datas.push(`${thisInput.name}=${thisInput.value}`);
            }
        }
        return { canSubmit, datas , selectData};
    };  
    var resetPass = function(){
        var form = document.querySelector("form#setPassForm");
        var inputs = form.querySelectorAll("input");
        var eachResult = eachInputs(inputs);
        if( eachResult.canSubmit ){
            var req = new XMLHttpRequest();
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);
                    if( result.answer ){
                        alert(result.message);
                        window.location.href = "/";
                    }else{
                        var m = document.querySelector("p.message-res");
                        m.innerText = result.message;
                    }
                }
            };
            req.open("POST", `/reset-password`, true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send(eachResult.datas.join("&"));
        }
    };
    var verfiyClick = function(){
        var form = document.querySelectorAll("form#accountForm");
        var inputs = accountForm.querySelectorAll("input");
        var eachResult = eachInputs(inputs, "account");
        
        if( eachResult.canSubmit ){
            var submitForm = document.querySelector("form#accountForm");
            var actionInput = document.querySelector("input[name='action']");
            var passForm = document.querySelector("form#setPassForm");
            var messager = document.querySelector("p.message");
            var Req = new XMLHttpRequest();
            
            Req.onreadystatechange = function(){
                if( Req.readyState == XMLHttpRequest.DONE && Req.status == 200 ){
                    var result = JSON.parse( Req.responseText );
                    if( result.answer ){
                        console.log(`actionInput.value : ${actionInput.value}`);
                        switch (actionInput.value) {
                            case "send-verify":
                                messager.innerText = result.message;
                                document.querySelector("div.checkCode").innerHTML = `<input class="form-control placeholder-no-fix" type="text" name="verify" placeholder="驗證碼"  />`;
                                actionInput.value = "auth-verify";                                
                                break;
                            case "auth-verify":
                                submitForm.style.display = "none";
                                document.querySelector("form.set-form").style.display = "block";
                                document.querySelector("button#passSubmit").addEventListener("click", resetPass);
                                document.querySelector("input#toReset").value = eachResult.selectData;
                                break;
                        }

                    }
                }   
            };
            Req.open("POST", `/${actionInput.value}`, true);
            Req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            Req.send(eachResult.datas.join("&"));
        }
    };
    // document.querySelector("button#passSubmit").addEventListener("click", resetPass);
    submit.addEventListener("click", verfiyClick);
})();