(function(){
    var deleteBtn = document.querySelector("a#deleteSelectRow");
    var deleteAll = document.querySelector("a#deleteAll");
    //選擇項目後「刪除」
    var handleDeleteBtnClick = function(){
        var idsInput = document.querySelectorAll("input[name='id[]']");
        var _crsf = document.querySelector("input[name='_csrf']").value;
        var _this = this;
        var ids = [];
        for( var k=0; k<idsInput.length; k++ ){
            var thisInput = idsInput[k];
            if( thisInput.checked ){
                ids.push(thisInput.value);
            }
        }
        if( ids.length > 0 && confirm( "是否確定刪除已選擇的資料？" ) ){
            var req = new XMLHttpRequest();
            var eachResult = [`_csrf=${_crsf}`, `id=${ids}`];
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);

                    if( result.answer ){
                        location.reload();
                    }else{
                        swal("刪除失敗！",result.message, "error");
                    }
                }
            };
            req.open("POST", `${_this.getAttribute("data-link")}`, true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send(eachResult.join("&"));
        }
    }
    //刪除「已刪除」項目
    var handleDeleteAll = function(){
        var _crsf = document.querySelector("input[name='_csrf']").value;
        var _this = this;
        var postLink = `/system/${_this.getAttribute("data-method")}/deleteAll`;
        console.log(_crsf);
        if( confirm( "此操作將會永遠刪除「已刪除」的項目，確定操作？" ) ){
            var req = new XMLHttpRequest();
            var eachResult = new FormData();
            eachResult.append("_csrf", _crsf);
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);
                    if( result.answer ){
                        location.reload();
                    }else{
                        swal("刪除失敗！",result.message, "error");
                    }
                }
            };
            req.open("POST", postLink, true);
            req.send(eachResult);
        }
    }
    if( deleteBtn ){
        deleteBtn.addEventListener("click", handleDeleteBtnClick);
    }
    if( deleteAll ){
        deleteAll.addEventListener("click", handleDeleteAll);
    }
})();