(function(){
    var form = document.querySelector("form");

    var checkInputContent = function(){
        var inputs = form.querySelectorAll("input");
        var text = form.querySelector("textarea[name='content']");
        var select = form.querySelector("select[name='to_department']");
        var errMessage ="";

        errMessage +=( text && text.value==="")? text.placeholder+", \n" : "";
        errMessage +=( select && select.value==="")? "必須選擇聯絡單對象部門, \n" : "";

        for(var k=0; k<inputs.length; k++){
            var thisInput = inputs[k];
            if( thisInput.getAttribute("data-request") === 'true' && !thisInput.value ){
                errMessage += thisInput.placeholder+",\n";
            }
        }

        if( !errMessage ){
            form.action = actionUrl;
            form.submit();
        }else{
            swal("有必填欄位空白。",errMessage, "error");
        }
    };
    if( form ){
        var submitBtn = form.querySelector(".submitBtn");
        var actionUrl = form.action;

        form.action = "";
        submitBtn.addEventListener("click", checkInputContent);
    }
    
})();