$(document).on("keypress", 'input', function (e) {
    var code = e.keyCode || e.which;
    if (code == 13) {
        e.preventDefault();
        return false;
    }
});
(function(){
    $("select.select2me").select2();
    if( $("#tags_1").size()>0 ){
        $("#tags_1").tagsInput({
            "height" : "150px",
            "width" : "250px"
        });
    }

    //keyword btn click
    var keywordBtn = document.querySelectorAll("a.keywordBtn");
    var handleKeywordClick = function(){
        var _this = this;
        
        if( _this.innerText ){
            var Req = new XMLHttpRequest();
            var _token = document.querySelector("meta[name='_token']");
            var data = [`keyword=${_this.innerText}`,`_csrf=${_token.content}`];

            Req.onreadystatechange = function(){
                if( Req.readyState == XMLHttpRequest.DONE && Req.status == 200 ){
                    var result = JSON.parse( Req.responseText );

                    if( result.data.length > 0 ){
                        var content = '';
                        for( var key in result.data ){
                            var thisRow = result.data[key];
                            content += `
                                <li>
                                    <div class="task-title">
                                        <span class="task-title-sp">
                                            <a href="/system/connection/show/${thisRow.id}" target="_blank"> 
                                                ${thisRow.title}
                                            </a>
                                        </span>
                                    </div>
                                </li>                            
                            `;
                        }
                        document.querySelector("span#keyword-title").innerText = `關鍵字「${_this.innerHTML}」相關聯絡單`;
                        document.querySelector("span#keyword-count").innerText = `${result.data.length} 筆`;
                        document.querySelector("ul#keyword-list").innerHTML = content;

                        $("#modal").modal({ showClose: false });
                    }

                    
                    if( result.message )  swal("查詢結果",result.message, "info"); 
                }   
            };
            Req.open("POST", `/system/connection/keyword`, true);
            Req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            Req.send(data.join("&"));
        }
    };
    if( keywordBtn.length > 0 ){
        for( var k=0; k<keywordBtn.length; k++ ){
            keywordBtn[k].addEventListener("click", handleKeywordClick);
        }
    }
})();
// var position = $(window).scrollTop();
// console.log(`start scrollTOp : ${position}`);
// $(window).scroll(function() {
//     var scroll = $(window).scrollTop();
//     console.log(`scroll scrollTOp : ${scroll}`);
//     if (scroll > position) {
//         console.log("下滾");
//     } else {
//         console.log("上滾");
//     }
//     position = scroll;
// });