(function(){
    var deleteBtn = document.querySelector("button#delete");
    var handleDeleteClick = function(){
        var _this = this;
        var id = document.querySelector("input[name='id']").value;
        var _crsf = document.querySelector("input[name='_csrf']").value;

        if( confirm( "是否確定刪除此筆資料？" ) ){
            var req = new XMLHttpRequest();
            var eachResult = [`_csrf=${_crsf}`, `id=${id}`];
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);

                    if( result.answer ){
                        alert(result.message);
                        window.location.href = _this.getAttribute("data-backLink");
                    }else{
                        swal("刪除帳號失敗！",result.message, "error");
                    }
                }
            };
            req.open("POST", `${_this.getAttribute("data-link")}`, true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send(eachResult.join("&"));
        }   
    }

    if( deleteBtn ){
        deleteBtn.addEventListener("click", handleDeleteClick);
    }
})();