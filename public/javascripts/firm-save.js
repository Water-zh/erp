(function(){
    var saveBtn = document.querySelector("button#save");

    //點擊退貨表單 
    var handleSave = function(){
        var form = document.querySelector("form#score");
        var inputs = form.querySelectorAll("input");
        var texts = form.querySelectorAll("textarea");
        
        var check = [];
        var datas = [];
        var message  = "";
        //檢查目前欄位是否有空白
        for( var k=0; k< inputs.length; k++ ){
            var thisInput = inputs[k];
            switch (thisInput.name) {
                case `returns`:
                    if( parseInt(thisInput.value) > parseInt(thisInput.getAttribute("max")) || parseInt(thisInput.value) < parseInt(thisInput.getAttribute("min")) ){
                        check.push(false);
                        message += "退貨數超過採購數量或小於零";
                    }
                    break;
                case `pay_back`:
                    if( parseInt(thisInput.value) > parseInt(thisInput.getAttribute("max")) || parseInt(thisInput.value) < parseInt(thisInput.getAttribute("min")) ){
                        check.push(false);
                        message += "補回數超過退貨數或小於零";
                    }
                    break;
            
                default:
                    if( thisInput.value == "" ) check.push(false);
                    break;
            }
            datas.push(`${thisInput.name}=${thisInput.value}`);
        }

        for( var t=0; t< texts.length; t++ ){
            var thisText = texts[t];
            datas.push(`${thisText.name}=${thisText.value}`);
            if( thisText.value == "" ) check.push(false);
        }

        if( check.length === 0 ){
            var req = new XMLHttpRequest();
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);
                    console.log( result );
                }
            }
            req.open("POST", `/system/firmscore/update`, true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send(datas.join("&"));
        }else{
            if( message === "" ) message = "有空白欄位，請補上資料！\n";
            alert(message);
        }

    }
    saveBtn.addEventListener("click", handleSave);
})();