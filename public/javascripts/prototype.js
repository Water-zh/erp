(function(){
    var searchBtn = document.querySelector("button.searchBtn");
    var msgBtn = document.querySelectorAll("a.setMessage");
    var listColors = ["aliceblue","snow"];

    function setImageEvent (){
        var imageBtn = document.querySelectorAll("a#showAll");
        if( imageBtn.length > 0 ){
            for(var k=0; k<imageBtn.length; k++){ imageBtn[k].addEventListener("click", handleImageClick); }
        }
    }
    function setActionAnswerEvent(){
        var answerBtns = document.querySelectorAll("a.actionAnswer"); 
        if( answerBtns.length > 0 ){
            for(var k=0; k<answerBtns.length; k++){ answerBtns[k].addEventListener("click", handleAnswerBtnClick); }
        }
    }
    //列表圖片點擊抓取所有圖片瀏覽
    function handleImageClick(){
        var postLink = '/system/prototype/showProductImages';
        var thisID = this.getAttribute("data-id");
        var ajaxData = new FormData();
        ajaxData.append("_csrf", document.querySelector("input[name='_csrf']").value);
        ajaxData.append("proto_id", thisID);
        var req = new XMLHttpRequest();
        req.onreadystatechange = function(){
            if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                var result = JSON.parse(req.responseText);        
                if( result.product.Photoes.length > 0 ){
                    var pswpContent = document.querySelector(".pswp");    
                    var gallery = new PhotoSwipe(pswpContent, PhotoSwipeUI_Default, result.product.Photoes);
                    gallery.init();
                }else{
                    swal("此產品沒有其他圖片","", "info");
                }
            }
        }
        req.open("POST", postLink, true);
        req.send(ajaxData);
    };
    //回應按鈕事件
    function handleAnswerBtnClick(){
        var _this = this;
        var postLink = '/system/prototype/sendStatus';
        var ajaxData = new FormData();
        ajaxData.append("_csrf", document.querySelector("input[name='_csrf']").value);
        ajaxData.append("id", _this.dataset.id);
        ajaxData.append("status", _this.dataset.type);
        var req = new XMLHttpRequest();
        req.onreadystatechange = function(){
            if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                var result = JSON.parse(req.responseText);
                swal("回應", result.message, result.type);
                //正常修改了才更改點擊按鈕樣式
                if( result.type == "info" ){
                    //顯示回應結果
                    var statusNum = parseInt(_this.dataset.type);
                    var clickParent = _this.parentNode;
                    var thisCellBtns = clickParent.querySelectorAll("a");
                    var statusColor = ["","","red","green","blue"];
                    var statusClass, changeBtn = "";
                    //移除所有原本會有的按鈕顏色，是點選的才加上
                    for(var k=0; k<thisCellBtns.length; k++){
                        var thisAbtn = thisCellBtns[k];
                        
                        thisAbtn.classList.remove(statusColor[thisAbtn.dataset.type]);
                        thisAbtn.classList.add("default");
                        if( parseInt(thisAbtn.dataset.type) == statusNum ){
                            statusClass = statusColor[statusNum];
                            changeBtn = thisAbtn;
                        }
                    }
                    changeBtn.classList.add(statusClass);
                }
            }
        }
        req.open("POST", postLink, true);
        req.send(ajaxData);
    }
    //搜尋事件
    var handleSearchClick = function(){
        var keyword = document.querySelector("input[name='keyword']");
        var desinger = document.querySelector("select[name='desinger']");
        if( keyword.value | desinger.value != 0 ){
            var _csrf = document.querySelector("input[name='_csrf']").value;
            var postLink = `/system/prototype/search`;
            var req = new XMLHttpRequest();
            var datas = new FormData();
            datas.append("_csrf", _csrf);
            datas.append("desinger", desinger.value);
            datas.append("keyword", keyword.value);
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);
                    var productList = document.querySelector("div#productList");
                    productList.innerHTML = result.html;
                    setImageEvent();
                    setActionAnswerEvent();
                }
            }
            req.open("POST", postLink, true);
            req.send(datas);
        }else{
            swal("必須輸入「關鍵字」或選擇特定設計師搜尋","", "error");
        }

    }
    var handleMessageBtn = function(){
        var _this = this;
        var id = _this.getAttribute("data-id");
        if(id !=""){
            var _csrf = document.querySelector("input[name='_csrf']").value;
            var messageForm = document.querySelector("form#send_message");
            var postLink = `/system/prototype/getMessages`;
            var req = new XMLHttpRequest();
            var datas = new FormData();

            messageForm.querySelector("input[name='id']").value = id;
            document.querySelector("button.sendMessageBtn").addEventListener("click", handleSendMessageBtn);
            datas.append("_csrf", _csrf);
            datas.append("id", id);
            
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);
                    var messageList = document.querySelector("div#messageList");
                    if(result.answer && result.datas.length >0){
                        var messageContent = "";
                        
                        for(var v=0; v<result.datas.length; v++){
                            var n = v %2;
                            var row = result.datas[v];
                            messageContent += `<div class="col-md-12" style="background-color:${listColors[n]}">${row.User.user_name}： ${row.message}</div>`;
                        }
                        messageList.innerHTML = messageContent;
                    }else{

                    }
                }
            }
            req.open("POST", postLink, true);
            req.send(datas);
        }
        
    }
    var handleSendMessageBtn = function(){
        var form = document.querySelector("form#send_message");
        var postLink = `/system/prototype/setMessages`;
        var req = new XMLHttpRequest();
        var datas = new FormData(form);
        
        req.onreadystatechange = function(){
            if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                var result = JSON.parse(req.responseText);
                
                swal(result.message,"", "info");
                if( result.answer == true ){
                    var messageList = document.querySelector("div#messageList");
                    var text = form.querySelector("textarea[name='content']");
                    var countList = messageList.querySelectorAll("div.col-md-12");
                    messageList.innerHTML += `<div class="col-md-12" style="background-color:${listColors[countList.length % 2]}">${result.userName}： ${text.value}</div>`; 
                }
            }
        }
        req.open("POST", postLink, true);
        req.send(datas);
    }
    setImageEvent();
    setActionAnswerEvent();
    searchBtn.addEventListener("click", handleSearchClick);
    if(msgBtn.length > 0){
        for (let v = 0; v < msgBtn.length; v++) {
            var thisMsg = msgBtn[v];
            thisMsg.addEventListener("click",handleMessageBtn);
        }
    }
})();