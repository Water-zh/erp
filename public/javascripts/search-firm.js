(function(){
    var sendBtn = document.querySelector("button.sendBtn"),
        select = document.querySelector("select#page"),//分頁下拉
        nowLink = `${window.location.origin}${window.location.pathname}`,
        startSearch = window.location.search;
    
    //監聽搜尋送出
    var handleSendClick = function(){
        var date = document.querySelector("input[name='date']");
        var poNumber = document.querySelector("input[name='poNumber']");
        var _csrf = document.querySelector("input[name='_csrf']");
        var loader = document.querySelector("div.loader");

        var req = new XMLHttpRequest();
        var datas = [`_csrf=${_csrf.value}`,`date=${date.value}`,`poNo=${poNumber.value}`];
        var _this = this;
        loader.style.display = "block";

        req.onreadystatechange = function(){
            if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                var result = JSON.parse(req.responseText);
                if( result.length > 0 ){

                    var list = document.querySelector("tbody[id='list']"),
                        pageCount = Math.ceil(result.dataCount / 15);

                    list.innerHTML = "";
                    for( var k=0; k<result.length; k++ ){
                        var thisRow = result[k];
                        list.innerHTML +=`
                            <tr>
                                <td class="numeric">${thisRow.TD024}</td>
                                <td class="numeric">${thisRow.TD012}</td>
                                <td class="numeric">${thisRow.TD004}</td>
                                <td class="numeric">${thisRow.TD005}</td>
                                <td class="numeric">${thisRow.TD006}</td>
                                <td class="numeric">${thisRow.TD008}</td>
                                <td class="numeric">${thisRow.TD009}</td>
                                <td class="numeric">
                                    <a class=" btn default" href="/system/firmscore/show/${thisRow.TD002}-${thisRow.TD004}" data-target="#detail" data-toggle="modal">輸入</a>
                                </td>
                            </tr>
                        `;
                    }

                    var hashString = `poNo=${poNumber.value}&date=${date.value}`;

                    window.history.pushState("", "", `${nowLink}?${hashString}`);
                    // getLinkParams();
                }else{
                    swal("搜尋結果",result.message, "info");
                }
                loader.style.display = "none";
            }
        };
        req.open("POST", `/system/firmscore/searchPurchase`, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.send(datas.join("&"));
    };
    sendBtn.addEventListener("click", handleSendClick);
    if( select ){
        select.addEventListener("change",handleSelectChange);
    }

})();
