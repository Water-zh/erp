(function(){
  var postLink = '/system/prototype/store';
  var form = document.querySelector("form#proStore");
  var sendBtn = document.querySelector("a.submitBtn");

  function sendStoreData(){
    var inputs = form.querySelectorAll("input");

    var message = '';
    //先檢查內容有沒有空白
    for(k in inputs){
      var thisInput = inputs[k];
      try {
        if(thisInput.getAttribute("data-request") == "true" && !thisInput.value ){
          message += `${thisInput.placeholder} \n`;
        }      
      } catch (error) { }
    }   
    //送出
    if( message === "" ){
      var ajaxData = new FormData(form);
      var req = new XMLHttpRequest();
      req.onreadystatechange = function(){
          if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
              var result = JSON.parse(req.responseText);
              // console.log( result );
              // var reader = new FileReader();
              if( result.answer ){
                window.location.href = "/system/prototype";
              }else{
                swal("新增時發生錯誤！",result.errMessage, "error");
              }
          }
      }
      req.open("POST", postLink, false);
      req.send(ajaxData);
    }else{
      swal("有必填欄位空白。",message, "error");
    }
  }
  sendBtn.addEventListener("click", sendStoreData);
})();
//5~6樣，未選取的設計用來參考可以。
//選兩樣 可以在修改
//用兩樣去延伸
//確認之後，會直接給風量跟馬達的博士去評估
//所以到時候出來是完整的兩台可以用的
//不需要我們另外來打模
