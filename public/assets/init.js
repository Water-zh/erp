(function(){
    Metronic.init();
    Layout.init(); // init layout

    //檢查是否有支援Notification
    if( ("Notification" in window) && 0 ){
        //先看看是否有權限
        if( Notification.permission !== "granted" ){
            Notification.requestPermission();
        }
    }
    //檢查service worker
    if( ("serviceWorker") in navigator && 0 ){
        var key, endpoint, authSecret;
        var _csrf = document.querySelector("meta[name='_csrf']");

        navigator.serviceWorker.register("/javascripts/notice.js", { scope : "/javascripts/" })
        .then(function(register){
            return register.pushManager.getSubscription().then(function(pushSubscription){
                if(pushSubscription){ 
                    return pushSubscription; 
                }
                return register.pushManager.subscribe({ userVisibleOnly : true });
            }).catch(err=>{ console.log(err) });
        }).then(function(subscribe){

            var jsonSub = subscribe.toJSON();;
            endpoint = jsonSub.endpoint;
            key = jsonSub.keys.p256dh;
            authSecret = jsonSub.keys.auth;
            var req = new XMLHttpRequest();
            var datas = [`_csrf=${_csrf.content}`,`endpoint=${endpoint}`, `key=${key}`, `authSecret=${authSecret}`];
            req.onreadystatechange = function(){
                if( req.readyState == XMLHttpRequest.DONE && req.status == 200 ){
                    var result = JSON.parse(req.responseText);
                }
            }
            req.open("POST", `/system/connection/sendNoticfication`, true);
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.send(datas.join("&"));
        });
    }
})();