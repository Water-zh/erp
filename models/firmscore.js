// let sequelize = require("sequelize");
let _seq = app.get('web_db');
let dataPrimary = require("../dataTablePrimary");

const FirmScore = _seq.define(
    "FirmScore", dataPrimary.FirmScore.column,
    { tableName : "firmscores", primaryKey : true }
);
const FirmScoreDetail = _seq.define(
    "FirmScoreDetail", dataPrimary.FirmScoreDetail.column,
    { tableName : "firmscore_details", primaryKey : true }
);

// FirmScore.hasMany(FirmScoreDetail, { foreignKey : "firm_id", as : "Details" });
FirmScore.hasOne(FirmScoreDetail, { foreignKey : "firm_id", as : "Details" });

FirmScoreDetail.belongsTo(FirmScore, { foreignKey : "firm_id", as : "FirmScore" });

exports.FirmScore = FirmScore;
exports.FirmScoreDetail = FirmScoreDetail;