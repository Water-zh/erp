const sequelize = require("sequelize");
const db = require("../controller/db");
const dataTablePrimary = require("../dataTablePrimary");
const Purtd = app.get("PURTD");
const Purtc = new db("PURTC", dataTablePrimary.PURTC.column);

var Purchase = Purtc._seq;
var PurchaseDetail = Purtd._seq;

// ConnectDepartment.belongsTo(Connect, { foreignKey : "connection_id", as : "Connect" });
PurchaseDetail.belongsTo(Purchase, { foreignKey : "TD002", as : "Connect" });