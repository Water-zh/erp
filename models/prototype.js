let sequelize = require("sequelize");
let _seq = app.get('web_db');
let dataPrimary = require("../dataTablePrimary");
// 產品設計分類
// const ProductDesignCategory = _seq.define(
//   "ProductDesignCategory", dataPrimary.ProductDesignCategory,
//   { tableName : "product_design_categories", primaryKey : true }
// );
//產品設計
const ProductDesign = _seq.define(
  "ProductDesign", dataPrimary.ProductDesign,
  { tableName : "product_designs", primaryKey : true }
);
//產品設計圖
const ProductDesignPhoto = _seq.define(
  "ProductDesignPhoto", dataPrimary.ProductDesignPhotos,
  { tableName : "product_design_photos", primaryKey : true }
);
//產品設計狀態更改紀錄
const ProductDesignStatusChange = _seq.define(
  "ProductDesignStatusChange", dataPrimary.ProductDesignStatusChange,
  { tableName : "product_design_status_change", primaryKey : true }
);
//產品設計權限
const ProductDesignPermission = _seq.define(
  "ProductDesignPermission", dataPrimary.ProductDesignPermission,
  { tableName : "product_design_permissions", primaryKey : true }
);
//產品設計討論訊息
const ProductDesignMessage = _seq.define(
  "ProductDesignMessage", dataPrimary.ProductDesignMessage,
  { tableName : "product_design_messages", primaryKey : true }
);
// ProductDesignCategory.hasMany(ProductDesign, { foreignKey : "cate_id", as : "Products" });
ProductDesign.hasMany(ProductDesignPhoto, { foreignKey : "proto_id", as : "Photos" });
ProductDesign.hasMany(ProductDesignStatusChange, { foreignKey : "proto_id", as : "Changes" });
ProductDesign.hasMany(ProductDesignMessage, { foreignKey : "proto_id", as : "Messages" });

// ProductDesign.belongsTo(ProductDesignCategory, { foreignKey : "cate_id", as : "Category" });
ProductDesignPhoto.belongsTo(ProductDesign, { foreignKey : "proto_id", as : "Product" });
ProductDesignStatusChange.belongsTo(ProductDesign, { foreignKey : "proto_id", as : "Changes" });
ProductDesignMessage.belongsTo(ProductDesign, { foreignKey : "proto_id", as : "Messages" });

// exports.ProductDesignCategory = ProductDesignCategory;
exports.ProductDesign = ProductDesign;
exports.ProductDesignPhoto = ProductDesignPhoto;
exports.ProductDesignStatusChange = ProductDesignStatusChange;
exports.ProductDesignPermission = ProductDesignPermission;
exports.ProductDesignMessage = ProductDesignMessage;