2017.10.25
新增
base.getDocxContent(file, Model, id)
讀取doc或docx檔案，並更新model id content 

2017.10.12
ms sql 2008 分頁
方法一：
適用於 SQL Server 2000/2005

SELECT TOP 頁大小 *
FROM table1
WHERE id NOT IN
          (
          SELECT TOP 頁大小*(頁數-1) id FROM table1 ORDER BY id
          )
ORDER BY id
方法二：
適用於 SQL Server 2000/2005

SELECT TOP 頁大小 *
FROM table1
WHERE id >
          (
          SELECT ISNULL(MAX(id),0) 
          FROM 
                (
                SELECT TOP 頁大小*(頁數-1) id FROM table1 ORDER BY id
                ) A
          )
ORDER BY id
方法三：
適用於 SQL Server 2005

SELECT TOP 頁大小 * 
FROM 
        (
        SELECT ROW_NUMBER() OVER (ORDER BY id) AS RowNumber,* FROM table1
        ) A
WHERE RowNumber > 頁大小*(頁數-1)

方法四：從第 M 筆資料開始取 N 筆 - SQL Server

SELECT *
　　FROM ( SELECT Top N *
　　FROM (SELECT Top (M + N - 1) * FROM table1 Order by id desc) t1 ) t2
　　Order by id asc

方法五：從第 M 筆資料開始取 N 筆 - Oralce

SELECT *
　　FROM (SELECT ROWNUM r,t1.* From table1 t1 where rownum < M + N) t2
　　where t2.r >= M

方法六：從第 M 筆資料開始取 N 筆 - MySQL

SELECT * FROM table1 LIMIT M,N

2017.7.31
傅協理提出，想要加入一個「關鍵字」的欄位，用來提示並搜尋相關歷史聯絡單出來，可以避免相關事項一再重複發送
2017.6.23
Leo提出聯絡單本身想要設定「時間點提醒」的部分，因為有可能今天發的單，不一定是這一兩天就會生產的
有可能生產時間點會過很久，所以需要把有設定今天提醒的聯絡單撈出來

2017.4.25 與erp端討論資料交流方式，初步決定erp將xlsx傳送到「dataFile」資料夾後，由我這邊去新增修改，假設有調整過資料，再匯出xlsx到「updateFiles」
讓erp更新資料到erp的資料庫中

2017.4.18完成app 流程需要的api

2017.3.31 修正controller/platn.js 「getPlantListByDate」
直接從sequelize.query下sql指令時，會重複得到資料，所以必須在query(``, { type: Sequelize.QueryTypes.SELECT})
這樣才不會重複返回相同資料
參考：http://stackoverflow.com/questions/33232147/sequelize-query-returns-same-result-twice
