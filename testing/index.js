'use strict';

const request = require('supertest');
const app = require('../app');

describe(`GET web path`, ()=>{
    it('active index page', (done)=>{
        request(app).get('/').timeout(3000).expect(200).end(done());
    });
    it(`post connection `, (done)=>{
        request(app)
            .post(`/system/connection/store`)
            .send({ 
                title : "從testing 測試store ",
                content : "test的內容",
                addfile : "~/Downloads/0.jpeg"
            })
            .end((err, res)=>{
                done();
            })
    })
});

// test('active index page', (t)=>{
    
// });