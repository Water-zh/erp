express = require('express');
var path = require('path');
// const mosca = require("mosca");
// var favicon = require('serve-favicon');
// var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mailer = require('express-mailer');
var http = require('http');
var session = require("express-session");
var csrf = require("csurf");
var Sequelize = require("sequelize");
var multer = require("multer");
const { moscaServer } =require("./controller/mosca");
const db = require('./controller/db');
const dataTablePrimary = require('./dataTablePrimary');

app = express(); // DEBUG=erp:* npm start  node.js-> express koa

// view engine setup
app.set("mosca", moscaServer);
app.set("isProduct", false);
app.set('views', path.join(__dirname, 'views'));
app.use('/static', express.static(__dirname + '/uploads'));
app.set('view engine', 'pug');
app.set('priviateKey', '+_|69^d7Tf;=3:29;46|.R7k');
app.set('priviateKey_web', 'F;=A4AcFY5UjWcW^==*A;n8Dj_+mxXYW');
app.set('MOCTA', new db(`MOCTA`, dataTablePrimary[`MOCTA`].column) );
app.set("PURTD", new db(`PURTD`, dataTablePrimary[`PURTD`].column ));
app.set("PURMA", new db(`PURMA`, dataTablePrimary[`PURMA`].column ));


app.locals.systemMenu = require('./systemMenu');
//for web sigma connect 
//server : Walter@sigma2017 : 33168
app.set("web_db", 
    new Sequelize("SIGMA", `root`, `PiglovedoG99`,{
        host : `localhost`,
        port: `3306`,
        dialect : "mysql",
        pool:{
            max: 100,
            min: 0,
            idle: 15000
        }
    }
));
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

fileUpload = multer({ dest : "./uploads/" });

mailer.extend(app, {
    from: 'zh@summer-wind.info',
    host: 'exch03.summer-wind.info', // hostname
    secureConnection: true, // use SSL
    port: 25, // port for secure SMTP
    transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
    auth: {
        user: 'zh@summer-wind.info',
        pass: 'May2017'
    }
});

app.use(session({
    secret: 'dsijewn2lk3-0djf332m4@ff',
    resave: false,
    saveUninitialized: true,
}));

csrfProtection = csrf();
parseForm = bodyParser.urlencoded({ extended: true });
app.use(bodyParser.json());
app.use(parseForm);
//routes 
require('./routes');

//watch 「dataFile」dir file change
// require('./controller/dirWatch');

//監看每天特定時間點要做的排程事件
require('./controller/schedule');

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


const server = http.createServer(app);
server.listen(8899);

module.exports = server;
