# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.19-MariaDB)
# Database: SIGMA
# Generation Time: 2018-02-02 09:08:54 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table product_design_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_design_permissions`;

CREATE TABLE `product_design_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account` int(11) NOT NULL COMMENT '[fk]使用者',
  `can_see` int(1) NOT NULL DEFAULT '0' COMMENT '可以看到產品設計功能',
  `is_designer` int(1) NOT NULL DEFAULT '0' COMMENT '是設計師',
  `can_answer` int(1) NOT NULL DEFAULT '0' COMMENT '可以設定回應',
  `updatedAt` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `product_design_permissions` WRITE;
/*!40000 ALTER TABLE `product_design_permissions` DISABLE KEYS */;

INSERT INTO `product_design_permissions` (`id`, `account`, `can_see`, `is_designer`, `can_answer`, `updatedAt`, `createdAt`)
VALUES
	(1,3,1,1,1,'2018-01-20 00:00:00','2018-01-20 00:00:00');

/*!40000 ALTER TABLE `product_design_permissions` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
